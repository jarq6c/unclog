#!/usr/bin/env python3

'''Compute baseflow parameters.'''

# Modules
from pathlib import Path
from datetime import timedelta as td
import pandas as pd
import numpy as np

# Frequency of data
_freq = td(minutes=5)

# Smooth time-series
def smooth_signal(ts, sp):
	df = pd.DataFrame()
	df['value'] = ts
	df['rvalue'] = df['value'].values[::-1]
	df['backward'] = df['rvalue'].ewm(
		span=sp, adjust=False).mean().values[::-1]
	df['forward'] = df['value'].ewm(span=sp, adjust=False).mean()
	return df[['backward', 'forward']].mean(axis=1)

# List events
def list_events(df):
	# Empty DataFrame
	el = pd.DataFrame()

	# Copy original DataFrame
	df2 = df.copy().reset_index()

	# Identify event starts
	df2['start'] = False
	df2['shift'] = df2['event'].shift(1).fillna(False)
	df2.loc[df2['event'] & ~df2['shift'], 'start'] = True
	el['start'] = df2.loc[df2.start, 'datetime'].values

	# Identify event stops
	df2['stop'] = False
	df2.loc[:, 'shift'] = (
		df2['event'].shift(-1).fillna(False))
	df2.loc[df2['event'] & ~df2['shift'], 'stop'] = True
	el['stop'] = df2.loc[df2.stop, 'datetime'].values

	# Compute durations
	el['duration'] = el['stop'].sub(el['start'])

	return el

# Main
if __name__ == '__main__':
	# Set document root
	root = Path(__file__).parents[1].resolve()

	# Input data
	dfile = root / 'data' / 'V02FORH_refined.pickle'

	# Load data
	df = pd.read_pickle(dfile)

	# List events
	el = list_events(df)

	# Characterize events
	#  The m-parameter is the amount of rise per time-step
	initial = df.loc[el.start, 'value'].values
	final = df.loc[el.stop, 'value'].values
	change = final - initial
	el.loc[:, 'duration'] = (el.duration / _freq).values
	el['m'] = change / el.duration

	# Save event list
	roel = el.copy(deep=True)
	roel['type'] = 'r'

	# Mark baseflow "events"
	df['runoff'] = df['event'].copy(deep=True)
	df.loc[:, 'event'] = ~df['runoff']

	# List events
	el = list_events(df)

	# Characterize events
	#  The m-parameter is the recession constant
	initial = df.loc[el.start, 'value'].values
	final = df.loc[el.stop, 'value'].values
	change = final - initial
	el.loc[:, 'duration'] = (el.duration / _freq).values
	el['m'] = np.log(initial/final) / el.duration
	el.loc[:, 'm'] = el['m'].fillna(-1.0)

	# Mark baseflow events
	el['type'] = 'b'

	# Save event lists
	ofile = root / 'data' / 'V02FORH_BF.pickle'
	el.to_pickle(ofile)
	ofile = root / 'data' / 'V02FORH_RO.pickle'
	roel.to_pickle(ofile)
