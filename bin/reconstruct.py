#!/usr/bin/env python3

'''Reconstruct signal with corrected baseflow.'''

# Modules
from pathlib import Path
from datetime import timedelta as td
import pandas as pd
import numpy as np

# Rolling minimum
def minimize_signal(ts, win):
	df = pd.DataFrame()
	df['value'] = ts
	df['rvalue'] = df['value'].values[::-1]
	df['backward'] = df['rvalue'].rolling(
		win).min().values[::-1]
	df['forward'] = df['value'].rolling(win).min()
	return df[['backward', 'forward']].max(axis=1)

# Main
if __name__ == '__main__':
	# Set document root
	root = Path(__file__).parents[1].resolve()

	# Input data
	dfile = root / 'data' / 'V02FORH_DFI.pickle'

	# Load data
	df = pd.read_pickle(dfile)

	# Identify base-flow trend
	df['trend'] = minimize_signal(df['value'], 'D')

	# Remove trend
	df['detrend'] = df['value'].sub(df['trend'])

	# Force zero median
	median = df['detrend'].median()
	df.loc[:, 'detrend'] = df['detrend'].sub(median)

	# Reconstruct baseflow
	df['corrected'] = df['detrend'].add(df['EVBF'])

	# Load runoff event data
	rfile = root / 'data' / 'V02FORH_ROC.pickle'
	rl = pd.read_pickle(rfile)

	# Note baseflow
	df['baseflow'] = True

	# Restore events
	for e in rl.itertuples():
		# Note baseflow
		df.loc[e.start:e.stop, 'baseflow'] = False

		# Find peak
		pktime = df.loc[e.start:e.stop, 'value'].idxmax()

		# Pre-peak
		ppk = pktime - td(minutes=5)

		# Pre-peak adjustment
		ppka = (df.loc[e.start, 'corrected'] - 
			df.loc[e.start, 'value'])

		# Place pre-peak
		df.loc[e.start:ppk, 'corrected'] = (
			df.loc[e.start:ppk, 'value'].add(ppka))

		# Post-peak adjustment
		popka = (df.loc[e.stop, 'corrected'] - 
			df.loc[e.stop, 'value'])

		# Place post-peak
		df.loc[pktime:e.stop, 'corrected'] = (
			df.loc[pktime:e.stop, 'value'].add(popka))

		# New peak value
		pkval = df.loc[pktime, 'corrected']

		# Retain peak
		(df.loc[(df.index >= e.start) & 
			(df.index <= ppk) & 
			(df.corrected >= pkval), 'corrected']) = np.nan

		# Fill values
		df.loc[e.start:e.stop, 'corrected'] = (
			df.loc[e.start:e.stop, 'corrected'].interpolate(
				method='time'))

	# Save
	ofile = root / 'data' / 'V02FORH_DFC.pickle'
	df[['value', 'locked', 'corrected', 'baseflow']].to_pickle(ofile)
	