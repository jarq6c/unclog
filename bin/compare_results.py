#!/usr/bin/env python3

'''Correct baseflow parameters.'''

# Modules
from pathlib import Path
import pandas as pd
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
from tools.figures import get_figure, write_figure, set_rcParams_sizes
from matplotlib.ticker import FormatStrFormatter
from matplotlib.ticker import MultipleLocator

# Use package style sheet
style_sheet = Path(__file__).parents[0] / 'tools/matplotlibrc'
style_sheet = style_sheet.resolve()
plt.style.use(str(style_sheet))

# Main
if __name__ == '__main__':
	# Set document root
	root = Path(__file__).parents[1].resolve()

	# Input data
	dfile = root / 'data' / 'V02FORH_DFCS.pickle'
	mfile = root / 'data' / 'V02FORH_MAN.pickle'

	# Load data
	df = pd.read_pickle(dfile)
	mf = pd.read_pickle(mfile)

	# Compute residuals
	df['residual'] = df['corrected'].sub(df['value'])

	# Decimate data for plotting
	df_ds = df.resample('6H').nearest()

	# Plotting ordinate
	x = np.linspace(0.0, 1000.0, 50)

	# Linear regression for scatter
	m, b, r_value, p_value, std_err = stats.linregress(
		df.value, df.corrected)

	# Stats
	mdy = df['corrected'].median()
	ymn = df['corrected'].mean()
	mdx = df['value'].median()
	xmn = df['value'].mean()

	# Labels
	raw_stats = ('$Md_U = {:.1f}$, '.format(mdx) + 
		'$\overline{x}_U = ' + 
		'{:.1f}$'.format(xmn))
	cor_stats = ('$Md_A = {:.1f}$, '.format(mdy) + 
		'$\overline{x}_A = ' + 
		'{:.1f}$'.format(ymn))

	# Plot options
	label = f'aut_residuals'
	caption = r'Scatter plot of uncorrected versus automatically (TSD) corrected stage data down-sampled to nearest 6-hour value to aid visualization.'

	# Get LaTex figure
	fig, ax = get_figure(size='large_square')

	# Plot
	ax.plot(df_ds.value, df_ds.corrected, '.k', markersize=1)
	ax.plot(x, x, label='1:1 line')
	ax.plot(x, b + m * x, linestyle='dashed', 
		label='y = {:.3}x + {:.3}, $r^2$={:.3}'.format(m, b, r_value**2))
	ax.plot([], [], color='white', label=raw_stats)
	ax.plot([], [], color='white', label=cor_stats)

	ax.grid(False)
	ax.set_aspect('equal')

	ax.xaxis.set_major_locator(MultipleLocator(200))
	ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
	ax.xaxis.set_minor_locator(MultipleLocator(100))

	ax.yaxis.set_major_locator(MultipleLocator(200))
	ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))
	ax.yaxis.set_minor_locator(MultipleLocator(100))

	ax.set_xlabel('Uncorrected Depth, $U$ (mm $H_2O$)')
	ax.set_ylabel('TSD Corrected Depth, $A$ (mm $H_2O$)')

	ax.set_xlim(0.0, 1000.0)
	ax.set_ylim(0.0, 1000.0)

	ax.legend()

	# Format plot and save
	write_figure(fig, label, caption)

	# Trim manual data
	start = df.index.min()
	stop = df.index.max()
	mf = mf.loc[start:stop, :]

	# Compute residuals
	mf['residual'] = mf['value'].sub(df['value'])

	# Decimate data for plotting
	mf_ds = mf.resample('6H').nearest()

	# Linear regression for scatter
	m, b, r_value, p_value, std_err = stats.linregress(
		df.value, mf.value)

	# Stats
	mdy = mf['value'].median()
	ymn = mf['value'].mean()
	mdx = df['value'].median()
	xmn = df['value'].mean()

	# Labels
	raw_stats = ('$Md_U = {:.1f}$, '.format(mdx) + 
		'$\overline{x}_U = ' + 
		'{:.1f}$'.format(xmn))
	cor_stats = ('$Md_M = {:.1f}$, '.format(mdy) + 
		'$\overline{x}_M = ' + 
		'{:.1f}$'.format(ymn))

	# Plot options
	label = f'man_residuals'
	caption = r'Scatter plot of uncorrected versus manually (MAN) corrected stage data down-sampled to nearest 6-hour value to aid visualization.'

	# Get LaTex figure
	fig, ax = get_figure(size='large_square')

	# Plot
	ax.plot(df_ds.value, mf_ds.value, '.k', markersize=1)
	ax.plot(x, x, label='1:1 line')
	ax.plot(x, b + m * x, linestyle='dashed', 
		label='y = {:.3}x + {:.3}, $r^2$={:.3}'.format(m, b, r_value**2))
	ax.plot([], [], color='white', label=raw_stats)
	ax.plot([], [], color='white', label=cor_stats)

	ax.grid(False)
	ax.set_aspect('equal')

	ax.xaxis.set_major_locator(MultipleLocator(200))
	ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
	ax.xaxis.set_minor_locator(MultipleLocator(100))

	ax.yaxis.set_major_locator(MultipleLocator(200))
	ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))
	ax.yaxis.set_minor_locator(MultipleLocator(100))

	ax.set_xlabel('Uncorrected Depth, $U$ (mm $H_2O$)')
	ax.set_ylabel('MAN Corrected Depth, $M$ (mm $H_2O$)')

	ax.set_xlim(0.0, 1000.0)
	ax.set_ylim(0.0, 1000.0)

	ax.legend()

	# Format plot and save
	write_figure(fig, label, caption)

	# Plot options
	label = f'residuals_hist'
	caption = r'Distributions of differences ($corrected - uncorrected$) for automatically (TSD) and manually (MAN) corrected stage data.'

	# Get LaTex figure
	fig, axs = get_figure(size='large_wide', ncols=2, nrows=1)

	# Set axis
	ax = axs.flat[0]

	# Plot
	df[['residual']].hist(bins=25, edgecolor='black', 
		grid=False, density=True, ax=ax, color='C5')

	ax.xaxis.set_major_locator(MultipleLocator(25))
	ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
	ax.xaxis.set_minor_locator(MultipleLocator(5))

	ax.yaxis.set_major_locator(MultipleLocator(0.02))
	ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
	ax.yaxis.set_minor_locator(MultipleLocator(0.005))

	ax.set_xlim(-50.0, 75.0)
	ax.set_ylim(0.0, 0.09)

	ax.set_title('(a)')
	ax.set_xlabel('TSD Difference (mm $H_2O$)')
	ax.set_ylabel('Relative Frequency')

	# Set axis
	ax = axs.flat[1]

	# Plot
	mf[['residual']].hist(bins=25, edgecolor='black', 
		grid=False, density=True, ax=ax, color='C9')

	ax.xaxis.set_major_locator(MultipleLocator(25))
	ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
	ax.xaxis.set_minor_locator(MultipleLocator(5))

	ax.yaxis.set_major_locator(MultipleLocator(0.02))
	ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
	ax.yaxis.set_minor_locator(MultipleLocator(0.005))

	ax.set_xlim(-50.0, 75.0)
	ax.set_ylim(0.0, 0.09)

	ax.set_title('(b)')
	ax.set_xlabel('MAN Difference (mm $H_2O$)')
	ax.set_ylabel('Relative Frequency')

	# Format plot and save
	write_figure(fig, label, caption)

	# Compare to field observations only
	lf = df.loc[df.locked, ['value', 'corrected', 'residual']]

	# Stats
	mdy = lf['corrected'].median()
	ymn = lf['corrected'].mean()
	mdx = lf['value'].median()
	xmn = lf['value'].mean()

	# Labels
	raw_stats = ('$Md_F = {:.1f}$, '.format(mdx) + 
		'$\overline{x}_F = ' + 
		'{:.1f}$'.format(xmn))
	cor_stats = ('$Md_A = {:.1f}$, '.format(mdy) + 
		'$\overline{x}_A = ' + 
		'{:.1f}$'.format(ymn))

	# Plotting ordinate
	x = np.linspace(0.0, 250.0, 50)

	# Linear regression for scatter
	m, b, r_value, p_value, std_err = stats.linregress(
		lf.value, lf.corrected)

	# Plot options
	label = f'aut_v_field'
	caption = r'Scatter plot of field observed versus automatically (TSD) corrected stage data.'

	# Get LaTex figure
	fig, ax = get_figure(size='large_square')
	
	ax.plot(lf.value, lf.corrected, 'ok', markersize=3)
	ax.plot(x, x, label='1:1 line')
	ax.plot(x, b + m * x, linestyle='dashed', 
		label='y = {:.3}x + {:.3}, $r^2$={:.3}'.format(m, b, r_value**2))
	ax.plot([], [], color='white', label=raw_stats)
	ax.plot([], [], color='white', label=cor_stats)
	ax.grid(False)
	ax.set_aspect('equal')

	ax.xaxis.set_major_locator(MultipleLocator(50))
	ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
	ax.xaxis.set_minor_locator(MultipleLocator(10))

	ax.yaxis.set_major_locator(MultipleLocator(50))
	ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))
	ax.yaxis.set_minor_locator(MultipleLocator(10))

	ax.set_xlabel('Field Observed Depth, $F$ (mm $H_2O$)')
	ax.set_ylabel('TSD Corrected Depth, $A$ (mm $H_2O$)')
	ax.legend()
	ax.set_xlim(0.0, 250.0)
	ax.set_ylim(0.0, 250.0)

	# Format plot and save
	write_figure(fig, label, caption)
