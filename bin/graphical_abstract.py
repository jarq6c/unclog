#!/usr/bin/env python3

'''Lightly smooth final signal.'''

# Modules
from pathlib import Path
from datetime import datetime as dt
from datetime import timedelta as td
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tools.figures import get_figure, write_figure
from matplotlib.ticker import FormatStrFormatter
from matplotlib.ticker import MultipleLocator

# Frequency of data
_freq = td(minutes=5)

# Use package style sheet
style_sheet = Path(__file__).parents[0] / 'tools/matplotlibrc'
style_sheet = style_sheet.resolve()
plt.style.use(str(style_sheet))

# Smooth time-series
def smooth_signal(ts, sp):
	df = pd.DataFrame()
	df['value'] = ts
	df['rvalue'] = df['value'].values[::-1]
	df['backward'] = df['rvalue'].ewm(
		span=sp, adjust=False).mean().values[::-1]
	df['forward'] = df['value'].ewm(span=sp, adjust=False).mean()
	return df[['backward', 'forward']].mean(axis=1)

# Rolling minimum
def minimize_signal(ts, win):
	df = pd.DataFrame()
	df['value'] = ts
	df['rvalue'] = df['value'].values[::-1]
	df['backward'] = df['rvalue'].rolling(
		win).min().values[::-1]
	df['forward'] = df['value'].rolling(win).min()
	return df[['backward', 'forward']].max(axis=1)

# Main
if __name__ == '__main__':
	# Set document root
	root = Path(__file__).parents[1].resolve()

	# Input data
	dfile = root / 'data' / 'V02FORH_DFCS.pickle'

	# Load data
	df = pd.read_pickle(dfile)

	# Smooth series
	df['hourly'] = df['value'].ewm(span=12, adjust=False).mean()

	# Isolate runoff event period
	start = dt(2016, 6, 9, 12)
	end = dt(2016, 6, 11, 12)
	dfr = df.loc[start:end, :].copy(deep=True)

	# Generate baseflow rise
	e_start = dt(2016, 6, 9, 19, 15)
	e_end = dt(2016, 6, 10, 19, 15)
	first = df.loc[e_start, 'hourly']
	last = df.loc[e_end, 'hourly']
	y = np.linspace(first, last, 289)
	dfr.loc[e_start:e_end, 'hourly'] = y

	# Fake some data
	adjust = dt(2016, 6, 9, 21, 25)
	nans = dt(2016, 6, 9, 21)
	nane = dt(2016, 6, 9, 21, 20)
	dfr['fake'] = dfr['value'].copy(deep=True)
	dfr['fake_hourly'] = dfr['hourly'].copy(deep=True)
	dfr.loc[adjust:, 'fake'] = dfr.loc[adjust:, 'fake'].sub(100)
	dfr.loc[adjust:, 'fake_hourly'] = dfr.loc[adjust:, 'fake_hourly'].sub(100)
	dfr.loc[nans:nane, 'fake'] = np.nan
	dfr.loc[:, 'fake'] = dfr['fake'].interpolate()

	# Generate baseflow rise
	first = dfr.loc[e_start, 'fake_hourly']
	last = dfr.loc[e_end, 'fake_hourly']
	y = np.linspace(first, last, 289)
	dfr.loc[e_start:e_end, 'fake_hourly'] = y

	# Plot options
	label = f'baseflow_rise_cor'
	caption = r'Storm-event water depth behind a weir over time. The erroneous record exhibits a lower depth after the storm event indicating the storm event cleared debris from the weir. Debris caused an artificial rise in water depth that emerged as a drop in depth after a storm event. The un-intuitive need to raise post-event depths (as opposed to lowering pre-event depths) is an artifact of using a pre-event field measurement to convert pressure sensor measurements to stage measurements.'

	# Smooth series
	df['daily'] = minimize_signal(df['value'], 'D')

	# Isolate baseflow recession
	start = dt(2017, 1, 12)
	end = dt(2017, 1, 27)
	dfb = df.loc[start:end, :].copy(deep=True)

	# Generate modeled baseflow
	df_locked = dfb[dfb.locked][['value']]
	initial = df_locked['value'].values[0]
	final = df_locked['value'].values[-1]
	change = final - initial
	duration = (df_locked.index[-1] - df_locked.index[0]) / _freq
	m = np.log(initial/final) / duration

	# Find origin
	x_o = (start - df_locked.index[0]) / _freq
	x_f = (end - df_locked.index[0]) / _freq
	x = np.arange(x_o, x_f+1)
	y = initial * np.exp(-m * x)
	dfb['modeled'] = y

	# Plot options
	label = f'graphical_abstract'
	caption = r'Ignore this figure'

	# Get LaTex figure
	fig, ax = get_figure(size='small')

	# Top plot
	dfb[['value', 'corrected', 'daily', 'modeled']].plot(ax=ax, 
		style=['-', '-', '--', '-.'])
	dfb.loc[dfb.locked, ['value']].plot(ax=ax, linestyle='', 
		marker='o', color='C4', markeredgecolor='black')
	ax.grid(False)
	ax.yaxis.set_major_formatter(
		FormatStrFormatter('%.1f'))
	ax.set_xlabel('Datetime (UTC)')
	ax.set_ylabel('Depth behind Weir (mm$H_2O$)')
	ax.legend(['Uncorrected Depth', 'Corrected Depth', 
		'Uncorrected Baseflow', 'Modeled Baseflow',
		'Field Observed Depth'])

	# Format plot and save
	write_figure(fig, label, caption)
