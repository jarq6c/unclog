#!/usr/bin/env python3

'''Correct baseflow parameters.'''

# Modules
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tools.figures import get_figure, write_figure, set_rcParams_sizes
from matplotlib.ticker import FormatStrFormatter
from matplotlib.ticker import MultipleLocator

# Use package style sheet
style_sheet = Path(__file__).parents[0] / 'tools/matplotlibrc'
style_sheet = style_sheet.resolve()
plt.style.use(str(style_sheet))

# Main
if __name__ == '__main__':
	# Set document root
	root = Path(__file__).parents[1].resolve()

	# Input data
	rfile = root / 'data' / 'V02FORH_RO.pickle'
	bfile = root / 'data' / 'V02FORH_BF.pickle'
	rcfile = root / 'data' / 'V02FORH_ROC.pickle'
	bcfile = root / 'data' / 'V02FORH_BFC.pickle'

	# Load data
	bl = pd.read_pickle(bfile)
	rl = pd.read_pickle(rfile)
	blc = pd.read_pickle(bcfile)
	rlc = pd.read_pickle(rcfile)

	# Load up iterables
	els = [bl, blc, rl, rlc]
	tps = ['b', 'b', 'r', 'r']
	scl = [10000, 10000, 100, 100]
	ys = ([
		np.linspace(-10., 15., 60),
		np.linspace(-10., 15., 60),
		np.linspace(-6., 10., 60),
		np.linspace(-6., 10., 60)
		])
	yls = ([
		(0, 11),
		(0, 11),
		(0, 13),
		(0, 13)
		])
	xls = ([
		(-11, 16),
		(-11, 16),
		(-7, 11),
		(-7, 11)
		])
	xts = ([
		np.arange(-10.0, 20.0, 5.0),
		np.arange(-10.0, 20.0, 5.0),
		np.arange(-6.0, 12.0, 3.0),
		np.arange(-6.0, 12.0, 3.0)
		])
	lbs = ([
		'(a) Uncorrected baseflow recessions', 
		'(b) Corrected baseflow recessions', 
		'(c) Uncorrected baseflow rises', 
		'(d) Corrected baseflow rises'
		])

	# Plot options
	label = f'bf_parameters'
	caption = r'Distributions of baseflow coefficients before and after automated correction.'

	# Get LaTex figure
	fig, axs = get_figure(size='large', nrows=2, ncols=2)

	# Plot
	for el, tp, sc, y, ax, yl, xl, xt, lb in zip(els, tps, scl, ys, axs.flat, yls, xls, xts, lbs):
		# Generate x
		x = (el.loc[el.type == tp[0], 'm'].values) * sc

		# Subplot
		if lb.startswith('Corrected'):
			ax.hist(x, y, edgecolor='black', 
				density=False, color='C5')
			e = str(int(np.floor(np.log10(sc))))
			ax.set_xlabel('Parameter' + r'$\times10^{' + e + r'}$')
		else:
			ax.hist(x, y, edgecolor='black', 
				density=False, color='C0')
			e = str(int(np.floor(np.log10(sc))))
			ax.set_xlabel('Parameter' + r'$\times10^{' + e + r'}$')
		ax.grid(False)
		ax.set_ylim(yl)
		ax.set_xlim(xl)
		x_labels = ['{:.1f}'.format(i) for i in xt]
		ax.set_xticks(xt)
		ax.set_xticklabels(x_labels)
		ax.set_ylabel('Number of Events')
		ax.set_title(lb)

	# Format plot and save
	write_figure(fig, label, caption)
