# Modules
from pathlib import Path
import pandas as pd

table_footer = '''

: CAPTION {#LABEL}
'''

# Write table
def write_table(df, name, caption, column_format=None, escape=True):
	# Set root and file name
	root = Path().cwd()
	ofile = root / ('tables/' + name + '.tex')
	label = 'tbl:' + name

	# Generate column format
	if not column_format:
		ncols = df.columns.size - 1
		column_format = 'L' + 'C' * ncols

	# Write table
	table = df.to_latex(index=False, column_format=column_format, 
		float_format='{:.2f}'.format, label=label, caption=caption, 
		escape=escape)

	# Reformat to tabularx environment
	table = table.replace(r'\begin{tabular}', r'\begin{tabularx}{\textwidth}')
	table = table.replace(r'\end{tabular}', r'\end{tabularx}')

	# Add placement specifier
	table = table.replace(r'\begin{table}', r'\begin{table}[H]')
	table = table.replace(r'\$', r'$')
	table = table.replace(r'pm', r'\pm')
	table = table.replace(r'alpha', r'$\alpha$')
	table = table.replace(r'beta', r'$\beta$')
	table = table.replace(r'gamma', r'$\gamma$')
	table = table.replace(r'textgreater', r'\textgreater')
	
	# Write file
	with ofile.open('w') as f:
		f.write(table)

	# Write to markdown
	if 'Index' in df:
		df = df.set_index('Index')
	md_table = df.to_markdown()

	# Add caption and label
	footer = table_footer.replace('CAPTION', caption)
	footer = footer.replace('LABEL', label)
	md_table += footer

	# Save snippet
	ofile = root / ('tables/' + name + '.md')
	with ofile.open('w') as f:
		f.write(md_table)
