#!/usr/bin/env python3

'''This script generates an Authorea compatible figure.'''

# Modules
from pathlib import Path
from io import StringIO
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import seaborn as sns
from pandas.plotting import register_matplotlib_converters

# Register converters
register_matplotlib_converters()

# Figure widths (inches)
small_width = 3.54
medium_width = 5.0
large_width = 7.25

# Quality (dots per inch)
dpi = 600

# Output formats
output_formats = ['pdf', 'png', 'eps']

# LaTEX template
template = r'''\begin{figure}PLACEMENT
\begin{center}
\includegraphics[width=\textwidth]{FIGPATH}
\caption{CAPTIONTEXT}
\label{LABELTEXT}
\end{center}
\end{figure}
'''

# Markdown template
md_template = '''![CAPTIONTEXT](FIGPATH){#LABELTEXT width=100%}'''

# Plot settings
current_palette = sns.color_palette('colorblind')
sns.set_palette(current_palette)

# Set rcParams
def set_rcParams_sizes(fontsize, linewidth, markersize):
	mpl.rcParams['font.size'] = fontsize
	mpl.rcParams['lines.linewidth'] = linewidth
	mpl.rcParams['grid.linewidth'] = linewidth * 0.6
	mpl.rcParams['axes.linewidth'] = linewidth * 0.6
	mpl.rcParams['lines.markersize'] = markersize

# Generate a figure of a given size
def get_figure(size='large', nrows=1, ncols=1, sharex=False, 
			gridspec_kw=None):
	if size == 'small':
		set_rcParams_sizes(6, 0.75, 3)
		figsize = (small_width, small_width*0.75)
	elif size == 'medium':
		set_rcParams_sizes(8, 1.0, 4)
		figsize = (medium_width, medium_width*0.75)
	elif size == 'small_wide':
		set_rcParams_sizes(6, 0.75, 3)
		figsize = (small_width, small_width*0.5625)
	elif size == 'medium_wide':
		set_rcParams_sizes(8, 1.0, 4)
		figsize = (medium_width, medium_width*0.5625)
	elif size == 'large_wide':
		set_rcParams_sizes(10, 1.5, 5)
		figsize = (large_width, large_width*0.5625)
	elif size == 'small_square':
		set_rcParams_sizes(6, 0.75, 3)
		figsize = (small_width, small_width)
	elif size == 'medium_square':
		set_rcParams_sizes(8, 1.0, 4)
		figsize = (medium_width, medium_width)
	elif size == 'large_square':
		set_rcParams_sizes(11, 1.6, 6)
		figsize = (large_width, large_width)
	else:
		set_rcParams_sizes(10, 1.5, 5)
		figsize = (large_width, large_width*0.75)

	return plt.subplots(nrows=nrows, ncols=ncols, figsize=figsize, dpi=dpi, sharex=sharex, 
			gridspec_kw=gridspec_kw)

# Save a figure
def write_figure(fig, name, caption, placement=r'[H]'):
	# Set root and file paths
	root = Path().cwd()
	odir = root / f'figures/{name}'
	odir.mkdir(exist_ok=True)
	label = 'fig:' + name

	# Write LaTEX snippet
	ext = output_formats[0]
	markup = template.replace('LABELTEXT', label)
	markup = markup.replace('CAPTIONTEXT', caption)
	markup = markup.replace('PLACEMENT', placement)
	markup = markup.replace('FIGPATH', f'figures/{name}/{name}.{ext}')

	# Write Markdown snippet
	markdown = md_template.replace('LABELTEXT', label)
	markdown = markdown.replace('CAPTIONTEXT', caption)
	markdown = markdown.replace('FIGPATH', f'figures/{name}/{name}.png')

	# Save files
	markup_file = odir / f'{name}.tex'
	with markup_file.open('w') as f:
		f.write(markup)
	markdn_file = odir / f'{name}.md'
	with markdn_file.open('w') as f:
		f.write(markdown)

	# Save plot
	for oformat in output_formats:
		ofile = odir / f'{name}.{oformat}'
		fig.savefig(ofile, bbox_inches='tight')
	