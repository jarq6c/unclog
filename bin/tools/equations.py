#!/usr/bin/env python3

'''This script generates a LaTex equation.'''

# Modules
from pathlib import Path

# Equation template
equation_template = r'''\begin{equation}
\label{llll}
    eeee
\end{equation}'''

# Write equation
def write_equation(equation, name):
	# Set root and file name
	root = Path().cwd()
	ofile = root / ('equations/' + name + '.tex')
	label = 'eqn:' + name

	# Generate equation string
	equation_string = equation_template.replace('llll', label)
	equation_string = equation_string.replace('eeee', equation)

	# Write to file
	with ofile.open('w') as f:
		f.write(equation_string)
	