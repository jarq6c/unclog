/* flow_calc.c
Program written by Fred Ogden 

Version 1.0 Sep. 2010-Jan. 2011 to calculate flows from V-notch concrete 
weirs.  fogden@uwyo.edu 

Version 2.1 April 2, 2011, modified to adjust weir angles as per calculations
by Trey Crouch of the as-built weir geometries. 

Version 3.0, October, 2015, to incorporate weir sedimentation study for ARS and Panama weir
crests.  Results completed for 120-degree ARS and Panama crest weirs only.  Also added ability
to use Cd values corrected for velocity head from our study flumes.   In general I don't 
recommend using the energy corrected values.

Version 3.1, October, 2015.  Used emperical fit equation by 
   Vatankhah and Peysokhan (J. Irrig. Drng. Eng. 2015) to calculate low-flow 90-degree sharp edge 
   triangular weir discharge coeffiecients as a function of the depth, h, above the weir invert. 
   Their equation was published in a comment on the paper by Bautista-Capetillo, et al. in the same
   journal.
   
Version 4.1 May 2016, by adding new results for Panama weir crests to the file weir_dat_May2016.h.
   Fixed bad behaviour for weir 7.  Program now works correctly for that weir.
   
Version 4.2 - Weir 12 and 13 added Nov. 25, 2017, based on field measurements provided by Jorge Batista, Nov. 2017. - FLO

*/

#include <stdio.h>
#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#define TRUE 1
#define FALSE 0
#define REDICULOUSLY_LARGE 1000000



extern double greg_2_jul(long year, long mon, long day, long h, long mi,
                         double se);
                         
extern void calc_date(double jd, long *y, long *m, long *d, long *h, long *mi,
                      double *sec);

extern double get_Cd_concrete_weir(int weir_num,int point_num,int yes_sedimented,int yes_correct, double HoverT,double *Hmax,int *quality_flag);

extern double Cd_interp(int point_num, double H, int n,double *h, double *c,int *warn);

extern void  Q_calc(int call_type, int yes_meters, int yes_correct,int site_num,int num_pts,double *wl,double *Q,
                    int *Q_quality_flag);

extern void read_file(FILE *in, char *fname, int callnum, int *nlines, int *filetype, int yes_one_val, int yes_temperature,
                      double *jdate, double *mm,int *new_flag,FILE *new_fptr);

extern void parse_line(char *theString,int filetype,int yes_one_val, int yes_temperature,int lnum,
                long *year,long *month, long *day,long *hour,
                long *minute, long *second,double *dat);
                
extern int find_site(char *str);

extern void sanity_check_and_open_files(char *ProgName,int *sitenum,
                            FILE **in_high_fptr, FILE **in_low_fptr, FILE **out_q_fptr,
                            FILE **out_jq_fptr,FILE **out_dq_fptr,FILE **out_jdq_fptr,
                            char *in_high_file_name,char *in_low_file_name,char *out_q_file_name,
                            char *out_jq_file_name,char *out_dq_file_name, char *out_jdq_file_name,
                            int yes_write_jdate_q,int yes_daily,
                            int yes_daily_jdate);


extern void find_best_Q(int site_num,double *depth_low,double *jdate_low,double *Q_low,double *jdate_high,
                        double *Q_high,double *Q_best,int num_lo,int num_hi,int yes,char *fname1,
                        char *fname2, double *Q_low_at_best, int *Q_high_quality_flag,int *Q_low_quality_flag,
                        int *Q_low_at_best_quality_flag);

extern void compute_daily_average_flows(int num_pts,double *Q,double *jdate,
                                   double **daily_jdate,double **daily_Q,int *numdays);

extern void write_output(int yes_high_weir,int yes_low_weir,int yes_write_both,
             double *jdate_low,double *jdate_high,double *Q_low,double *Q_high,double *Q_best,
             double *Qlow_at_best,
             int num_high_pts,int num_low_pts,char *out_q_file_name,
             char *out_jq_file_name,char *out_dq_file_name,char *out_jdq_file_name,
             FILE *out_q_fptr,FILE *out_jq_fptr,FILE *out_dq_fptr,FILE *out_jdq_fptr,
             int num_high_days,double *daily_jdate,double *daily_Q_high,
             int *Q_high_quality_flag,int *Q_low_quality_flag,int *Q_low_at_best_quality_flag,
             char *in_high_file_name,char *in_low_file_name,int argc,char **argv);

extern void get_word(char *theString,int *start,int *end,char *theWord,int *wordlen);

extern void itwo_alloc( int ***ptr, int x, int y);
extern void dtwo_alloc( double ***ptr, int x, int y);
extern void d_alloc(double **var,int size);
extern void i_alloc(int **var,int size);
extern void usage(char *name);

/*######################################################################*/
/*############################# MAIN PROGRAM ###########################*/
/*######################################################################*/

int main(int argc, char *argv[])
{
FILE *in_high_fptr;
FILE *in_low_fptr;
FILE *out_q_fptr;
FILE *out_jq_fptr;
FILE *out_dq_fptr;
FILE *out_jdq_fptr;
FILE *out_new_fptr;

char *ProgName=NULL;
char *arg=NULL;
char *in_high_file_name=NULL;
char *in_low_file_name=NULL;
char *out_q_file_name=NULL;
char *out_jq_file_name=NULL;
char *out_dq_file_name=NULL;
char *out_jdq_file_name=NULL;


int yes_write_jdate_q=FALSE; 
int yes_daily = FALSE;
int yes_daily_jdate=FALSE;
int yes_high_weir=FALSE;
int yes_low_weir=FALSE;
int yes_write_both=FALSE;

int num_high_pts=0;  /* the number of data points in the hi flow weir water level file     */
int num_low_pts=0;   /* ... low flow weir */
int filetype;   /* 1 if mm/dd/yyyy hh:mm:ss AM/PM water level or         */
                /* 2 if mm/dd/yyyy hh:mm water level with 24 hour clock. */
                /* 3 if mm/dd/yyyy hh:mm:ss AM/PM baro data or           */
                /* 4 if mm/dd/yyyy hh:mm:ss baro data file.              */

int i,j,len,callnum;
int yes_one_val=FALSE;
int yes_temperature=FALSE;
int yes_write_new_file=FALSE;
int yes_meters=FALSE;
int yes_correct=FALSE;
int *Q_high_quality_flag=NULL;
int *Q_low_quality_flag=NULL;
int *Q_low_at_best_quality_flag=NULL;

/* Q_quality flags are as follows:
-1 there are no low-flow weir data at this time
0- all ok
1- low flow weir box overtopped
2- high-flow weir depth > (H/T=3.5), which is the end of the lab data 
3- high-flow weir beyond max. depth of V  */


double *jdate_high=NULL;  /* array for julian date of hi-flow data points */
double *jdate_low=NULL;   /* ditto for low flow points */
double *depth_high=NULL; /* holds water levels at high flow weir */
double *depth_low=NULL;  /* ..ditto for low flow weir */
double *Q_high=NULL;   /* array of Q's calculated for high flow weir */
double *Q_low=NULL;    /* array of Q's calculated for low flow weir */
double *Q_best=NULL;   /* array to store the "best" discharge estimate if high and low weirs present */
                       /* these will correspond to the same times as jdate_high */
double *Q_low_at_best=NULL; /* Store interpolated Qlow value if both high and low values are specified */
                           /* for writing to the julian date output file if yes_write_both==FALSE   */
double *daily_Q_low=NULL; /* array of Q's to store the daily averages */
double *daily_Q_high=NULL; /* array of... high */
double *daily_jdate=NULL; /* array of julian dates to just store the YYYYMMDD */
double start_jdate;  /* the start of the common data period between hi- and low-flow weirs */
double end_jdate;    /* the end of the common data period between hi- and low-flow weirs */
int num_d=0;         /* number of "best estimate" flow points */
double dt,min_dt,nearestQ,sum,jday,jday_old,jdiff,press;
int jdex;
int site_num,fred;
int num_high_days,num_low_days,call_type;
long year,month,day,hour,minute,second;
double dsec;

ProgName=argv[0];
if(argc<4) usage(ProgName);  /* not enough arguments given */
/* in_file_name=argv[1]; */

/* parse the command line arguments */
for(i=1; i < argc; i++)
  {
  arg = argv[i];
  if(arg[0] == '-') 
    {
    switch (arg[1]) 
      {
      /*
   fprintf (stderr, "usage:  %s [-options ...]\n\n", ProgName);
   fprintf (stderr, "where options include:\n");
   fprintf (stderr, "   -l  <filename> input low-flow weir water level (mm) file <filename>\n");
   fprintf (stderr, "   -h  <filename> input high-flow weir water level (mm) file <filename>\n");
   fprintf (stderr, "   -o  <filename> write flows to the file <filename>, MM/DD/YYYY format\n");
   fprintf (stderr, "   -j  <filename> write flows to the file <filename>, Julian date format\n");
   fprintf (stderr, "   -d  <filename> write daily average Q to file <filename>, MM/DD/YYYY format\n");
   fprintf (stderr, "   -D  <filename> write daily average Q to file <filename>, Julian date format\n");
   fprintf (stderr, "   -s  write both high and low flow series to file, separated by '&'\n");
   fprintf (stderr, "   -c  use Cd values corrrected for velocity head.  NOT RECOMMENDED\n");
   fprintf (stderr, "   -m  flag indicating that depths in the input file(s) are in meters!\n\n");
   fprintf (stderr, "NOTES:\n");
   fprintf (stderr, "1.  The 'best' flow value is only calculated if the -h and -l flags are used.\n");
   fprintf (stderr, "2.  If the depth in the weir box is less than the spill depth, the smallest\n");
   fprintf (stderr, "    of the high- and low-flow weir flows is assumed to be correct.\n");
   fprintf (stderr, "3.  The files specified by either -o or -j contain in the first column\n");
   fprintf (stderr, "    the 'best' flow, followed by the low-flow weir box flow in the second.\n");
   fprintf (stderr, "4.  The daily average output stuff is not yet working- FLO Nov. 28, 2010\n");
      */

      case 'l':     /* read low-flow weir water levels from file specified */          
        if (++i >= argc) usage (ProgName);
        in_low_file_name = argv[i];
        yes_low_weir=TRUE;
        continue;
      case 'h':     /* read high-flow weir water levels file specified */          
        if (++i >= argc) usage (ProgName);
        in_high_file_name = argv[i];
        yes_high_weir=TRUE;
        continue;
      case 'o':     /* write flows to file specified in MM/DD/YYYY HH:MM:SS Qbest Qlow Qhigh format */          
        if (++i >= argc) usage (ProgName);
        out_q_file_name = argv[i];
        continue;
      case 'j':     /* write out discharges to a file with jdate Qbest Qlow Qhigh format */          
        if (++i >= argc) usage (ProgName);
        out_jq_file_name = argv[i];
        continue;
      case 'd':     /* write daily discharges to file specified  MM/DD/YYYY HH:MM:SS Qbest Qlow Qhigh*/          
        if (++i >= argc) usage (ProgName);
        out_dq_file_name = argv[i];
        yes_daily = TRUE;
        continue;
      case 'D':     /* write daily discharges to file specified JDATE Qbest Qlow Qhigh */          
        if (++i >= argc) usage (ProgName);
        out_jdq_file_name = argv[i];
        yes_daily_jdate = TRUE;
        continue;
      case 's':     /* write out discharges to a file with jdate Qbest Qlow Qhigh format */          
        yes_write_both=TRUE;
        continue;
      case 'c':     /* use Cd corrected for velocity head.  Not recommended in general, and impossible for W4 and W6. */
                    /* this would only be recommended if you enter total H, and your channel is similar to our flumes */          
        yes_correct=TRUE;
        continue;
      case 'm':     /* input depths are in meters. */          
        yes_meters=TRUE;
        continue;
      default:
        usage (ProgName);
      }
    }
  if(i >=argc) usage(ProgName);
  }

/*########################################################*/
/* check that at options are feasible.  If so, open files */
/*########################################################*/
sanity_check_and_open_files(ProgName, &site_num,&in_high_fptr, &in_low_fptr, &out_q_fptr,
                            &out_jq_fptr,&out_dq_fptr,&out_jdq_fptr,
                            in_high_file_name,in_low_file_name,out_q_file_name,
                            out_jq_file_name,out_dq_file_name,out_jdq_file_name,
                            yes_write_jdate_q,yes_daily,
                            yes_daily_jdate);

/* hard coded options */
yes_one_val=TRUE;       /* all these files have only one value */
yes_temperature=FALSE;  /* none of these files have temperature values */
yes_write_new_file=FALSE;  /* we don't want to do this in this program */

/*######################*/
/* read the input files */
/*######################*/
if(in_high_file_name!=NULL)
  {
  callnum=1;  /* callnum==1 means just read the file and return number of lines */
  read_file(in_high_fptr,in_high_file_name,callnum,&num_high_pts,&filetype,yes_one_val,
            yes_temperature,jdate_high,depth_high,&yes_write_new_file,out_new_fptr);
  printf("the high flow weir water level data file contains a total of %d lines\n",num_high_pts);
  if(num_high_pts<REDICULOUSLY_LARGE)
    {
    /* if num_pts is not rediculously large */
    d_alloc(&jdate_high,num_high_pts);
    d_alloc(&depth_high,num_high_pts);
    d_alloc(&Q_high,num_high_pts);
    i_alloc(&Q_high_quality_flag,num_high_pts);
    }
  callnum=2;
  rewind(in_high_fptr);
  read_file(in_high_fptr,in_high_file_name,callnum,&num_high_pts,&filetype,yes_one_val,yes_temperature,
            jdate_high,depth_high,&yes_write_new_file,out_new_fptr);
          
  printf("of which %d lines are data.\n",num_high_pts);
  fclose(in_high_fptr);
  call_type=1;
 
  Q_calc(call_type,yes_meters,yes_correct,site_num,num_high_pts,depth_high,Q_high,Q_high_quality_flag);
  
  /* Q_calc(call_type,yes_meters,yes_correct,site_num,num_high_pts,depth_high,Q_high,Q_high_quality_flag); */
  }
if(in_low_file_name!=NULL)
  {
  callnum=1;  /* callnum==1 means just read the file and return number of lines */
  read_file(in_low_fptr,in_low_file_name,callnum,&num_low_pts,&filetype,yes_one_val,
            yes_temperature,jdate_low,depth_low,&yes_write_new_file,out_new_fptr);
  printf("the low flow weir water level data file contains a total of %d lines\n",num_low_pts);
  if(num_low_pts<REDICULOUSLY_LARGE)
    {
    /* if num_low_pts is not rediculously large */
    d_alloc(&jdate_low,num_low_pts);
    d_alloc(&depth_low,num_low_pts);
    d_alloc(&Q_low,num_low_pts);
    i_alloc(&Q_low_quality_flag,num_low_pts);
    }
  callnum=2;
  rewind(in_low_fptr);
  read_file(in_low_fptr,in_low_file_name,callnum,&num_low_pts,&filetype,yes_one_val,yes_temperature,
            jdate_low,depth_low,&yes_write_new_file,out_new_fptr);
          
  printf("of which %d lines are data.\n",num_low_pts);
  fclose(in_low_fptr);
  call_type=2;  /* low flow weir */
  Q_calc(call_type,yes_meters,yes_correct,site_num,num_low_pts,depth_low,Q_low,Q_low_quality_flag);
  }

if(yes_high_weir && yes_low_weir && !yes_write_both)  /* come up with the "best" discharge estimate */
  {
  /* this method assumes that the high flow weir is the time-stamp to preserve, so we don't */
  /* get rid of peaks!  That means that Q_best is estimated for the same times as jdate_high */
  d_alloc(&Q_best,num_high_pts); 
  d_alloc(&Q_low_at_best,num_high_pts); 
  i_alloc(&Q_low_at_best_quality_flag,num_high_pts); 

  find_best_Q(site_num,depth_low,jdate_low,Q_low,jdate_high,Q_high,Q_best,num_low_pts,num_high_pts,
              yes_write_both,out_q_file_name,out_jq_file_name,Q_low_at_best,
              Q_high_quality_flag,Q_low_quality_flag,Q_low_at_best_quality_flag);
  }

write_output(yes_high_weir,yes_low_weir,yes_write_both,jdate_low,jdate_high,Q_low,Q_high,Q_best,
             Q_low_at_best,
             num_high_pts,num_low_pts,out_q_file_name,out_jq_file_name,out_dq_file_name,out_jdq_file_name,
             out_q_fptr,out_jq_fptr,out_dq_fptr,out_jdq_fptr,num_high_days,daily_jdate,daily_Q_high,
             Q_high_quality_flag,Q_low_quality_flag,Q_low_at_best_quality_flag,
             in_high_file_name,in_low_file_name,argc,argv);
 
  
return(0);
}

/*#######################################################################*/
/*########################### FIND BEST Q ###############################*/
/*#######################################################################*/
void find_best_Q(int site_num,double *depth_low,double *jdate_low,double *Q_low,double *jdate_high,
                 double *Q_high,double *Q_best,int num_lo,int num_hi,int yes_write_both,
                 char *out_q_file_name,char *out_jq_file_name,double *Q_low_at_best,
                 int *Q_high_quality_flag,int *Q_low_quality_flag,int *Q_low_at_best_quality_flag)
                 
{
int i,j,k,low_start_dex,dex,fred,bad_low,bad_high,flag;
double jstart_low,jstart_high,jstop_low,jstop_high,jnow,t_ratio;
double hlow_now,Qlow_now;

/* Q_quality flags are determined in Q_calc() and are as follows:
-1 there are no low-flow weir data at this time.
0- all ok
1- low flow weir box overtopped
2- high-flow weir depth > (H/T=3.5 for 120deg or 2.87 for 140deg), which is the end of the lab data 
3- high-flow weir beyond max. depth of V  
5- both 2 and 3 are true */

jstart_low=jdate_low[1];
jstop_low=jdate_low[num_lo];
jstart_high=jdate_high[1];
jstop_high=jdate_high[num_hi];

low_start_dex=(-1);
if(jstart_low<jstart_high)
  {
  /* find the starting index just before the high flow weir data start */
  for(i=1;i<num_lo;i++)
    {
    if(jdate_low[i]<jstart_high && jdate_low[i+1]>=jstart_high)
      {
      low_start_dex=i;
      break;
      }
    }
  if(low_start_dex<0)
    {
    printf("Did not find overlapping data period between low flow and high-flow weirs in\n");
    printf("find_best_Q().  Something is wrong with your data.   Program stopped.\n");
    exit(-89);
    }
  }
else low_start_dex=1;

dex=low_start_dex;
for(i=1;i<=num_hi;i++)
  {
  /* set these guys to be no data in case there isn't any low-flow data for this point in time */
  Q_best[i]=Q_high[i]; /* let's start with these assumptions, and see if it is larger than Q_low_now */
  Q_low_at_best_quality_flag[i]=-1;
  Q_low_at_best[i]=0.0;

  jnow=jdate_high[i];

  
  if(jnow>=jstart_low && jnow<=jstop_low)  /* there is low-flow weir data to compare against */
    {
    /* interpolate low flow data in time to find the depth and Q at time jnow */
    while(jnow>jdate_low[dex+1] && dex<num_lo)
      {
      dex++;  /* increment, we have gone beyond the span of the low-flow data */
      }
    if(dex>num_lo)
      {
      printf("Major problem in find_best_Q.  Program has run out of low flow data prematurely.\n");
      printf("Program stopped.\n\n");
      exit(-79);
      }
    bad_low=FALSE;
    bad_high=FALSE;
    if(Q_high_quality_flag[i]>0) bad_high=TRUE;  /* this happens when H<T is less than 0.7 or when overtopped */
    if(Q_low_quality_flag[dex]>0 || Q_low_quality_flag[dex+1]>0) 
      {
      bad_low=TRUE;  /* this happens when box overflows */
      Q_low_at_best_quality_flag[i]=1;
      }
    if(bad_low==FALSE)
      {
      t_ratio=(jnow-jdate_low[dex])/(jdate_low[dex+1]-jdate_low[dex]);
      hlow_now=depth_low[dex]+(depth_low[dex+1]-depth_low[dex])*t_ratio;
      Qlow_now=Q_low[dex]+(Q_low[dex+1]-Q_low[dex])*t_ratio;
      if(yes_write_both==FALSE && (out_q_file_name!=NULL || out_jq_file_name!=NULL))
        {
        flag=0;
        if(Q_low_quality_flag[dex]>0 || Q_low_quality_flag[dex+1]>0) flag=1;
        Q_low_at_best[i]=Qlow_now;
        Q_low_at_best_quality_flag[i]=flag;
        }
      if(Qlow_now<Q_high[i] || bad_high==TRUE) Q_best[i]=Qlow_now;  /* the lower value should be more accurate/?? */
      }
    if(Q_best[i]<0.0)
      {
      fred=1;
      printf("Flow negative in find_best_Q().  Serious problem.\n");
      }
    }
  }
return;
}      

/*#######################################################################*/
/*######################### FIND SITE ###################################*/
/*#######################################################################*/
int find_site(char *str)
{
int i;
char site_name[50][7];

strcpy(site_name[1],"v01as");
strcpy(site_name[2],"v02as");
strcpy(site_name[3],"v03as");
strcpy(site_name[4],"v04as");
strcpy(site_name[5],"v05as");  /* weir in silvipastoral catchment.  Has no weir box. */
strcpy(site_name[6],"v06as");
strcpy(site_name[7],"v07as");
strcpy(site_name[8],"v08as");
strcpy(site_name[9],"v09as");
strcpy(site_name[10],"v10sp");  /* this is the real weir 10, 'sp' because it is located in Soberania N.P. */ 
strcpy(site_name[11],"v11as");  /* weir at 14 ha cut and non-cut catchment.    Has no weir box.  */
strcpy(site_name[12],"v12as");  /* weir at 7 ha cut catchment.    Has weir box.  */
strcpy(site_name[13],"v13as");  /* weir in old growth forrest near Arnulfo camp.    Has weir box.  */
/*#########################################################################################*/
strcpy(site_name[14],"v14as"); /* there is no such weir.  placeholder */
strcpy(site_name[15],"v15as"); /* there is no such weir.  placeholder */
strcpy(site_name[16],"v16as"); /* there is no such weir.  placeholder */
strcpy(site_name[17],"v17as"); /* there is no such weir.  placeholder */
strcpy(site_name[18],"v18as"); /* there is no such weir.  placeholder */
strcpy(site_name[19],"v19as"); /* there is no such weir.  placeholder */
strcpy(site_name[20],"v10as"); /* there is no such weir.  placeholder */
/*#########################################################################################*/
/* THESE WEIRS EXIST BUT DATA FOR THEM DO NOT YET EXIST */
strcpy(site_name[21],"v01ca"); /* weir 1 in ciudad del arbol */
strcpy(site_name[22],"v02ca"); /* weir 2 in ciudad del arbol */
strcpy(site_name[23],"v03ca"); /* weir 3 in ciudad del arbol */
strcpy(site_name[24],"v04ca"); /* weir 4 in ciudad del arbol */
strcpy(site_name[25],"v01cp"); /* weir 1 in cerro pelado */
strcpy(site_name[26],"v02cp"); /* weir 2 in cerro pelado */

for(i=1;i<=26;i++)
  {
  if(strncmp(site_name[i],str,5)==0)
  return(i);
  }

printf("Site number not found in site_find(). File or Folder name must start\n");
printf("with a name like 'V06AS'.  This is how the program identifies the\n");
printf("weir number (e.g. Agua Salud weir #6).  Program stopped.\n\n");
exit(-2);
}
/*#######################################################################*/
/*########################## WRITE OUTPUT ###############################*/
/*#######################################################################*/
void write_output(int yes_high_weir,int yes_low_weir,int yes_write_both,
             double *jdate_low,double *jdate_high,double *Q_low,double *Q_high,double *Q_best,
             double *Q_low_at_best,
             int num_high_pts,int num_low_pts,char *out_q_file_name,
             char *out_jq_file_name,char *out_dq_file_name,char *out_jdq_file_name,
             FILE *out_q_fptr,FILE *out_jq_fptr,FILE *out_dq_fptr,FILE *out_jdq_fptr,
             int num_high_days, double *daily_jdate,double *daily_Q_high,
             int *Q_high_quality_flag,int *Q_low_quality_flag,int *Q_low_at_best_quality_flag,
             char *in_high_file_name,char *in_low_file_name,int argc,char **argv)
{
int i,len;
long year,month,day,hour,minute,second;
double dsec;
char *jstr;
 
if(out_q_file_name!=NULL)
  {
  fprintf(out_q_fptr,"# Results of: \n#");
  for(i=0;i<argc;i++) fprintf(out_q_fptr," %s ",argv[i]);
  fprintf(out_q_fptr,"\n#\n");
  if(yes_high_weir && yes_low_weir && !yes_write_both)
    {
    fprintf(out_q_fptr,"# Results of processing files: %s and %s\n",in_high_file_name,in_low_file_name);
    fprintf(out_q_fptr,"# Data quality flags are:\n");
    fprintf(out_q_fptr,"# 0- all ok\n");
    fprintf(out_q_fptr,"#-1- there are no low-flow weir data at this time\n");
    fprintf(out_q_fptr,"# 1- low flow weir box overtopped\n");
    fprintf(out_q_fptr,"# 2- high-flow weir depth > (H/T=3.5 for 120deg or 2.87 for 140deg),\n");
    fprintf(out_q_fptr,"#    which is the end of the lab data\n");
    fprintf(out_q_fptr,"# 3- high-flow weir beyond max. depth of V\n");
    fprintf(out_q_fptr,"# 5- both 2 and 3 are true\n#\n");
    fprintf(out_q_fptr,"#                                                  Low    High\n");
    fprintf(out_q_fptr,"#                                Low       High    Flow   Flow\n");
    fprintf(out_q_fptr,"#                        Best   Flow       Flow    Qual.  Qual.\n");
    fprintf(out_q_fptr,"# MM/DD/YYYY HH:MM:SS   Q Est.  Weir Q    Weir Q   Flag   Flag\n");
    fprintf(out_q_fptr,"# -------------------------------------------------------------\n");

    /* write out the "best" Q value determined by find_best_Q() */
    for(i=1;i<=num_high_pts;i++)
      {
      calc_date(jdate_high[i],&year,&month,&day,&hour,&minute,&dsec);
      second=(long)dsec;
      fprintf(out_q_fptr,"%2.2ld/%2.2ld/%4.4ld %2.2ld:%2.2ld:%2.2ld %8.4lf %8.4lf %8.4lf     %1d     %1d\n",
                          month,day,year,hour,minute,second,Q_best[i],Q_low_at_best[i],Q_high[i],
                          Q_low_at_best_quality_flag[i],Q_high_quality_flag[i]);
      }
    }
  if(yes_high_weir && yes_low_weir && yes_write_both)
    {
    fprintf(out_q_fptr,"# Results of processing files: %s and %s\n",in_high_file_name,in_low_file_name);
    fprintf(out_q_fptr,"# Data quality flags are:\n");
    fprintf(out_q_fptr,"# 0- all ok\n");
    fprintf(out_q_fptr,"#-1- there are no low-flow weir data at this time\n");
    fprintf(out_q_fptr,"# 1- low flow weir box overtopped\n");
    fprintf(out_q_fptr,"# 2- high-flow weir depth > (H/T=3.5 for 120deg or 2.87 for 140deg),\n");
    fprintf(out_q_fptr,"#    which is the end of the lab data\n");
    fprintf(out_q_fptr,"# 3- high-flow weir beyond max. depth of V\n");
    fprintf(out_q_fptr,"# 5- both 2 and 3 are true\n#\n");
    fprintf(out_q_fptr,"#                                 Low\n");
    fprintf(out_q_fptr,"#                        Low      Flow\n");
    fprintf(out_q_fptr,"#                        Flow     Qual.\n");
    fprintf(out_q_fptr,"# MM/DD/YYYY HH:MM:SS    Weir Q   Flag\n");
    fprintf(out_q_fptr,"# -------------------------------------\n");


    for(i=1;i<=num_low_pts;i++)
      {
      calc_date(jdate_low[i],&year,&month,&day,&hour,&minute,&dsec);
      second=(long)dsec;    
      fprintf(out_q_fptr,"%2.2ld/%2.2ld/%4.4ld %2.2ld:%2.2ld:%2.2ld %8.4lf %1d\n",
                          month,day,year,hour,minute,second,Q_low[i],Q_low_quality_flag[i]);
      }
    fprintf(out_q_fptr,"&\n");
    fprintf(out_q_fptr,"#                                 High\n");
    fprintf(out_q_fptr,"#                        High     Flow\n");
    fprintf(out_q_fptr,"#                        Flow     Qual.\n");
    fprintf(out_q_fptr,"# MM/DD/YYYY HH:MM:SS    Weir Q   Flag\n");
    fprintf(out_q_fptr,"# -------------------------------------\n");
    for(i=1;i<=num_high_pts;i++)
      {
      calc_date(jdate_high[i],&year,&month,&day,&hour,&minute,&dsec);
      second=(long)dsec;    
      fprintf(out_q_fptr,"%2.2ld/%2.2ld/%4.4ld %2.2ld:%2.2ld:%2.2ld %8.4lf %1d\n",
                          month,day,year,hour,minute,second,Q_high[i],Q_high_quality_flag[i]);
      }
    }
  if(yes_high_weir && !yes_low_weir)
    {
    fprintf(out_q_fptr,"# Results of processing high flow weir file: %s \n",in_high_file_name);
    fprintf(out_q_fptr,"# Data quality flags are:\n");
    fprintf(out_q_fptr,"# 0- all ok\n");
    fprintf(out_q_fptr,"#-1- there are no low-flow weir data at this time\n");
    fprintf(out_q_fptr,"# 1- low flow weir box overtopped\n");
    fprintf(out_q_fptr,"# 2- high-flow weir depth > (H/T=3.5 for 120deg or 2.87 for 140deg),\n");
    fprintf(out_q_fptr,"#    which is the end of the lab data\n"); 
    fprintf(out_q_fptr,"# 3- high-flow weir beyond max. depth of V\n");
    fprintf(out_q_fptr,"# 5- both 2 and 3 are true\n#\n");
    fprintf(out_q_fptr,"#                                 High\n");
    fprintf(out_q_fptr,"#                        High     Flow\n");
    fprintf(out_q_fptr,"#                        Flow     Qual.\n");
    fprintf(out_q_fptr,"# MM/DD/YYYY HH:MM:SS    Weir Q   Flag\n");
    fprintf(out_q_fptr,"# -------------------------------------\n");
    for(i=1;i<=num_high_pts;i++)
      {
      calc_date(jdate_high[i],&year,&month,&day,&hour,&minute,&dsec);
      second=(long)dsec;
      
      fprintf(out_q_fptr,"%2.2ld/%2.2ld/%4.4ld %2.2ld:%2.2ld:%2.2ld %8.4lf %1d\n",
                          month,day,year,hour,minute,second,Q_high[i],Q_high_quality_flag[i]);
      }
    }     
  if(!yes_high_weir && yes_low_weir)
    {
    fprintf(out_q_fptr,"# Results of processing low flow weir file: %s \n",in_low_file_name);
    fprintf(out_q_fptr,"# Data quality flags are:\n");
    fprintf(out_q_fptr,"# 0- all ok\n");
    fprintf(out_q_fptr,"#-1- there are no low-flow weir data at this time\n");
    fprintf(out_q_fptr,"# 1- low flow weir box overtopped\n");
    fprintf(out_q_fptr,"# 2- high-flow weir depth > (H/T=3.5 for 120deg or 2.87 for 140deg),\n");
    fprintf(out_q_fptr,"#    which is the end of the lab data\n");
    fprintf(out_q_fptr,"# 3- high-flow weir beyond max. depth of V\n");
    fprintf(out_q_fptr,"# 5- both 2 and 3 are true\n#\n");
    fprintf(out_q_fptr,"#                                 Low\n");
    fprintf(out_q_fptr,"#                        Low      Flow\n");
    fprintf(out_q_fptr,"#                        Flow     Qual.\n");
    fprintf(out_q_fptr,"# MM/DD/YYYY HH:MM:SS    Weir Q   Flag\n");
    fprintf(out_q_fptr,"# -------------------------------------\n");
    for(i=1;i<=num_low_pts;i++)
      {
      calc_date(jdate_low[i],&year,&month,&day,&hour,&minute,&dsec);
      second=(long)dsec;
      
      fprintf(out_q_fptr,"%2.2ld/%2.2ld/%4.4ld %2.2ld:%2.2ld:%2.2ld %8.4lf %1d\n",
                          month,day,year,hour,minute,second,Q_low[i],Q_low_quality_flag[i]);
      }
    }
  fclose(out_q_fptr);
  }

if(out_jq_file_name!=NULL)
  {
  fprintf(out_jq_fptr,"# Results of: \n#");
  for(i=0;i<argc;i++) fprintf(out_jq_fptr," %s ",argv[i]);
  fprintf(out_jq_fptr,"\n#\n");
  if(yes_high_weir && yes_low_weir && !yes_write_both)
    {
    /* write out the "best" Q value determined by find_best_Q(), followed by the low value */
    fprintf(out_jq_fptr,"# Results of processing files: %s and %s\n",in_high_file_name,in_low_file_name);
    fprintf(out_jq_fptr,"# Data quality flags are:\n");
    fprintf(out_jq_fptr,"# 0- all ok\n");
    fprintf(out_jq_fptr,"#-1- there are no low-flow weir data at this time\n");
    fprintf(out_jq_fptr,"# 1- low flow weir box overtopped\n");
    fprintf(out_jq_fptr,"# 2- high-flow weir depth > (H/T=3.5 for 120deg or 2.87 for 140deg),\n");
    fprintf(out_jq_fptr,"#    which is the end of the lab data\n");
    fprintf(out_jq_fptr,"# 3- high-flow weir beyond max. depth of V\n");
    fprintf(out_jq_fptr,"# 5- both 2 and 3 are true\n#\n");
    fprintf(out_jq_fptr,"#                                       Low    High\n");
    fprintf(out_jq_fptr,"# Strict                 Low    High    Flow   Flow\n");
    fprintf(out_jq_fptr,"# Julian          Best   Flow   Flow    Qual.  Qual.\n");
    fprintf(out_jq_fptr,"#  Date          Q Est.  Weir Q Weir Q  Flag   Flag\n");
    fprintf(out_jq_fptr,"#-------------------------------------------------------------\n");
    for(i=1;i<=num_high_pts;i++)
      {
      fprintf(out_jq_fptr,"%13.6lf %8.4lf %8.4lf %8.4lf   %1d   %1d\n",jdate_high[i],Q_best[i],Q_low_at_best[i],Q_high[i],
                           Q_low_at_best_quality_flag[i],Q_high_quality_flag[i]);
      }
    }
  if(yes_high_weir && yes_low_weir && yes_write_both)
    {
    fprintf(out_jq_fptr,"# Results of processing files: %s and %s\n",in_high_file_name,in_low_file_name);
    fprintf(out_jq_fptr,"# Data quality flags are:\n");
    fprintf(out_jq_fptr,"# 0- all ok\n");
    fprintf(out_jq_fptr,"#-1- there are no low-flow weir data at this time\n");
    fprintf(out_jq_fptr,"# 1- low flow weir box overtopped\n");
    fprintf(out_jq_fptr,"# 2- high-flow weir depth > (H/T=3.5 for 120deg or 2.87 for 140deg),\n");
    fprintf(out_jq_fptr,"#    which is the end of the lab data\n");
    fprintf(out_jq_fptr,"# 3- high-flow weir beyond max. depth of V\n");
    fprintf(out_jq_fptr,"# 5- both 2 and 3 are true\n#\n");
    fprintf(out_jq_fptr,"#                         Low\n");
    fprintf(out_jq_fptr,"# Strict         Low      Flow\n");
    fprintf(out_jq_fptr,"# Julian         Flow     Qual.\n");
    fprintf(out_jq_fptr,"#  Date          Weir Q   Flag\n");
    fprintf(out_jq_fptr,"#-------------------------------------\n");
    for(i=1;i<=num_low_pts;i++)
      {
      fprintf(out_jq_fptr,"%13.6lf %8.4lf   %1d\n",jdate_low[i],Q_low[i],Q_low_at_best_quality_flag[i]);
      }
    fprintf(out_jq_fptr,"&\n");
    fprintf(out_jq_fptr,"#                         High\n");
    fprintf(out_jq_fptr,"# Strict         High     Flow\n");
    fprintf(out_jq_fptr,"# Julian         Flow     Qual.\n");
    fprintf(out_jq_fptr,"#  Date          Weir Q   Flag\n");
    fprintf(out_jq_fptr,"#-------------------------------------\n");

    for(i=1;i<=num_high_pts;i++)
      {
      fprintf(out_jq_fptr,"%13.6lf %8.4lf   %1d\n",jdate_high[i],Q_high[i],Q_high_quality_flag[i]);
      }
    }
  if(yes_high_weir && !yes_low_weir)
    {
    fprintf(out_jq_fptr,"# Results of processing file: %s\n",in_high_file_name);
    fprintf(out_jq_fptr,"# Data quality flags are:\n");
    fprintf(out_jq_fptr,"# 0- all ok\n");
    fprintf(out_jq_fptr,"#-1- there are no low-flow weir data at this time\n");
    fprintf(out_jq_fptr,"# 1- low flow weir box overtopped\n");
    fprintf(out_jq_fptr,"# 2- high-flow weir depth > (H/T=3.5 for 120deg or 2.87 for 140deg),\n");
    fprintf(out_jq_fptr,"#    which is the end of the lab data\n");
    fprintf(out_jq_fptr,"# 3- high-flow weir beyond max. depth of V\n");
    fprintf(out_jq_fptr,"# 5- both 2 and 3 are true\n#\n");
    fprintf(out_jq_fptr,"#                         High\n");
    fprintf(out_jq_fptr,"# Strict         High     Flow\n");
    fprintf(out_jq_fptr,"# Julian         Flow     Qual.\n");
    fprintf(out_jq_fptr,"#  Date          Weir Q   Flag\n");
    fprintf(out_jq_fptr,"#-------------------------------------\n");

    for(i=1;i<=num_high_pts;i++)
      {
      fprintf(out_jq_fptr,"%13.6lf %8.4lf   %1d\n",jdate_high[i],Q_high[i],Q_high_quality_flag[i]);
      }
    }     
  if(!yes_high_weir && yes_low_weir)
    {
    fprintf(out_jq_fptr,"# Results of processing file: %s\n",in_low_file_name);
    fprintf(out_jq_fptr,"#   Note- A high flow weir file was not specified.\n");
    fprintf(out_jq_fptr,"# Data quality flags are:\n");
    fprintf(out_jq_fptr,"# 0- all ok\n");
    fprintf(out_jq_fptr,"#-1- there are no low-flow weir data at this time\n");
    fprintf(out_jq_fptr,"# 1- low flow weir box overtopped\n");
    fprintf(out_jq_fptr,"# 2- high-flow weir depth > (H/T=3.5 for 120deg or 2.87 for 140deg),\n");
    fprintf(out_jq_fptr,"#    which is the end of the lab data\n");
    fprintf(out_jq_fptr,"# 3- high-flow weir beyond max. depth of V\n");
    fprintf(out_jq_fptr,"# 5- both 2 and 3 are true\n#\n");
    fprintf(out_jq_fptr,"#                         Low\n");
    fprintf(out_jq_fptr,"# Strict         Low      Flow\n");
    fprintf(out_jq_fptr,"# Julian         Flow     Qual.\n");
    fprintf(out_jq_fptr,"#  Date          Weir Q   Flag\n");
    fprintf(out_jq_fptr,"#-------------------------------------\n");

    for(i=1;i<=num_low_pts;i++)
      {
      fprintf(out_jq_fptr,"%13.6lf %8.4lf   %1d\n",jdate_low[i],Q_low[i],Q_low_quality_flag[i]);
      }
    }
  fclose(out_jq_fptr);
  }

if(out_jdq_file_name!=NULL)
  {
  compute_daily_average_flows(num_high_pts,Q_high,jdate_high,&daily_jdate,&daily_Q_high,&num_high_days);
  
  for(i=0;i<num_high_days;i++) 
    {
    fprintf(out_jdq_fptr,"%13.6lf %8.4lf\n",daily_jdate[i],daily_Q_high[i]);
    }
  fclose(out_jdq_fptr);
  }
return;
}

/*#######################################################################*/
/*################### COMPUTE DAILY AVERAGE FLOWS #######################*/
/*#######################################################################*/
void compute_daily_average_flows(int num_pts,double *Q,double *jdate,
                                 double **daily_jdate,double **daily_Q,
                                 int *num)
{
int i,j,jdex;
int num_days,num_d;
double start_jdate,end_jdate,sum,jday,jdiff,jday_old;

start_jdate=jdate[1];
end_jdate=jdate[num_pts];
num_days=(end_jdate-start_jdate)+1;
d_alloc(daily_Q,num_days);
d_alloc(daily_jdate,num_days);
num_days=0;

/*  double jday,jday_old,jdiff;
    int jdex;  */
    
j=0;
sum=0.0;
jday_old=start_jdate-0.5;
jdex=0;
for(i=1;i<=num_pts;i++)
  {
  jday=jdate[i]-(double)0.5;
  jdiff=jday-jday_old;
  if(jdiff>1.0) 
    {
    /* the day changed */
    (*daily_Q)[j]=sum/(double)num_d;
    (*daily_jdate)[j]=jday+(double)0.50000000001-1.0;
    jday_old=jday;
    sum=0.0;
    num_d=0;
    j++;
    }
  else
    {
    sum+=Q[i];
    num_d++;
    }
  jdex=(int)(jday-(start_jdate-0.5)); 
  }
*num=j-1;
return;
}

/*#######################################################################*/
/*############################# Q CALC ##################################*/
/*#######################################################################*/
void Q_calc(int calltype,int yes_meters,int yes_correct,int site_num, int num_pts, double *depth,double *Q,
            int *Q_quality_flag)
{
/* Q_quality flags are as follows:
0- all ok
1- low flow weir box overtopped
2- high-flow weir H/T<0.7
3- both 1 and 2
4- water behind high flow weir above top of V
8- high flow weir depth above the highest Cd value, Q unreliable.
99- this is weir 7 and the V-notch is being overtopped.
100- this is weir 7 and everything is being overtopped, so we don't know what the flow is */


double neg_h,h;
int i;
int yes_high=FALSE;
int yes_low=FALSE;
int yes_full_sediment;

double Cd_low,Cd_high,grav,coeff,HoT;
double weir_angle[20],low_flow_weir_angle;
double h_max[20];   /* max. depth (m) with flow within high-flow V-notch weir */
double h_flat[20];  /* max. depth (m) on the flat portion of a weir above V, if it has one */
double T_weir[20];  /* thickness of concrete weir wall, (m) */
double box_max[20]; /* the maximum depth of water before box overtopping above invert of steel plate V (m) */
double HoTmax[20]; /* the max. H/T for which water is still entirely inside the  weir */
double b_flat,Cd_flat;

Cd_low=0.59;
grav=9.81;

for(i=1;i<=num_pts;i++) Q_quality_flag[i]=0;  /* initialize */

if(calltype==1) yes_high=TRUE;
if(calltype==2) yes_low=TRUE;   /* this is  used only for weirs without a high flow box. */

/* these parameters come from the data in spreadsheet named "re-survey_weirs.xls" */

/* angle of V           depth of V    max H in weir box  thickness of weir   max depth above V
    (degrees)              (m)         above weir invert   concrete wall      on flat portion
                                               (m)             (m)              of weir wall (m) */
weir_angle[1]=142.93;  h_max[1]=2.45;  box_max[1]=0.544;  T_weir[1]=0.38;    h_flat[1]=0.0;
weir_angle[2]=142.66;  h_max[2]=1.79;  box_max[2]=0.329;  T_weir[2]=0.38;    h_flat[2]=0.0;
weir_angle[3]=126.08;  h_max[3]=2.17;  box_max[3]=0.297;  T_weir[3]=0.38;    h_flat[3]=0.0;
weir_angle[4]=122.66;  h_max[4]=0.57;  box_max[4]=0.297;  T_weir[4]=0.20;    h_flat[4]=0.5000;
weir_angle[5]=122.56;  h_max[5]=1.49;  box_max[5]=0.000;  T_weir[5]=0.20;    h_flat[5]=0.31;
weir_angle[6]=120.18;  h_max[6]=0.92;  box_max[6]=0.322;  T_weir[6]=0.21;    h_flat[6]=0.2945;
weir_angle[7]=87.9;    h_max[7]=0.74;  box_max[7]=0.590;  T_weir[7]=0.0;     h_flat[7]=0.59;
weir_angle[8]=117.12;  h_max[8]=1.49;  box_max[8]=0.332;  T_weir[8]=0.21;    h_flat[8]=0.3095;
weir_angle[9]=119.59;  h_max[9]=1.28;  box_max[9]=0.340;  T_weir[9]=0.21;    h_flat[9]=0.2840;
weir_angle[10]=120.89; h_max[10]=1.46; box_max[10]=0.300; T_weir[10]=0.305;  h_flat[10]=0.2865;
weir_angle[11]=121.4;  h_max[11]=0.73; box_max[11]=0.000; T_weir[11]=0.21;   h_flat[11]=0.30;  /*FIXME- h_flat unknown */
weir_angle[12]=120.1;  h_max[12]=0.75; box_max[12]=0.300; T_weir[12]=0.21;   h_flat[12]=0.30;  /*FIXME- h_flat unknown */
weir_angle[13]=121.4;  h_max[13]=0.73; box_max[13]=0.300; T_weir[13]=0.21;   h_flat[13]=0.30;  /*FIXME- h_flat unknown */


for(i=1;i<=11;i++) HoTmax[i]=h_max[i]+h_flat[i];  /* CHANGE THIS IF MORE WEIRS ADDED */


if(site_num<1 || site_num>13 )
  {
  printf("Program does not know how to compute flows for weir %d\n",site_num);
  printf("Program stopped.\n");
  exit(-98);
  }

/* calculate discharges */
for(i=1;i<=num_pts;i++)
  {
  if(yes_meters==TRUE) h=depth[i];         /* depths are already in meters */
  else                 h=depth[i]/1000.0;  /* convert from mm to meters */
  if(yes_high)
    {
    HoT=h/T_weir[site_num]; /* HoT stands for "H over T", where H is the depth and T is the */
                            /* thickness of the concrete weir wall */
    if(HoT<0.7) Q_quality_flag[i]+=2;  /* comparison of 8"thick Panama concrete weirs indicates that when H/T<0.7, concrete weir doesn't work well */
    Cd_high=get_Cd_concrete_weir(site_num,i,yes_full_sediment,yes_correct,HoT,HoTmax,Q_quality_flag); 
    coeff=Cd_high*8.0/15.0*sqrt(2.0*grav)*tan(weir_angle[site_num]*M_PI/180.0/2.0);
    }
  else if(yes_low)
    {
    /* Cd_low=0.59; This was used in flow calc before version 3.1 */
    /*Vatankhah and Peysokhan, J. Irrig. Drain. Engr., 10.1061/(ASCE)IR.1943-4774.0000683 (2015) */
    /******EXAMPLE CALCULATED VALUES BY Cd_low EQUATION BELOW ********/
    /*      h(m)   Cd   h(m)   Cd   h(m)  Cd 
            0.01 0.680  0.11 0.629  0.21 0.615
            0.02 0.666  0.12 0.627  0.22 0.614
            0.03 0.657  0.13 0.625  0.23 0.613
            0.04 0.651  0.14 0.624  0.24 0.612
            0.05 0.646  0.15 0.622  0.25 0.611
            0.06 0.642  0.16 0.621  0.30 0.606
            0.07 0.639  0.17 0.619  0.35 0.603
            0.08 0.636  0.18 0.618  0.40 0.600
            0.09 0.634  0.19 0.617  0.45 0.597
            0.10 0.631  0.20 0.616  0.55 0.592
     *********************************************/
    if(site_num!=7) low_flow_weir_angle=90.0;
    else            low_flow_weir_angle=weir_angle[7];
    
    Cd_low=1.25-0.678*pow(h,0.0357)*pow((1.0+0.06*sin(1.45+2.156*(low_flow_weir_angle*M_PI/180.0))),0.15);

    coeff=Cd_low*8.0/15.0*sqrt(2.0*grav)*tan(low_flow_weir_angle*M_PI/180.0/2.0);
    if(h>0.95*box_max[site_num])
      {
      Q_quality_flag[i]+=1;  /* box is being overtopped, I assume at depths equal or greater than 95% full */
      }
    }
  Q[i]=coeff*pow(h,(double)2.5);
  
  /*@@@@@@@@@@@ Do special stuff for weir 7- it is the only steel-plate only weir @@@@@@@@@@@*/
  if(site_num==7 && h>box_max[site_num] )
    {
    if(h<=h_max[site_num])
      {
      /* here I assume that when the water depth exceeds the depth of the 'V', then the V continues on up, and I reduce the crest length of */
      /* the flat short-crested weir.   I'm not sure exactly what else to do .  This is kinda ball-parky ok.... */
      b_flat=232.5-(h-box_max[site_num])*2.0*cos(M_PI/180.0*(weir_angle[site_num]));  /* this is the width of the flat part not consumed by the V */
      h-=box_max[site_num];  /* this is how many m over the top of the V the water is. */
      Q[i]+=1.61*b_flat/100.0*pow(h,1.5);
      Q_quality_flag[i]=99;
      }
    else
      {
      Q[i]=-10.0;
      Q_quality_flag[i]=100;
      }
    }
  }
return;
}

/*#######################################################################*/
/*################## ALL THE LAB TEST DATA ARE USED HERE ################*/
/*#######################################################################*/
extern double get_Cd_concrete_weir(int weir_num,int point_num,int yes_sedimented,int yes_correct,double HoverT,double *HoTmax,int *quality_flag)
{
/* 
The following weirs have ARS crests:  1, 2, 3.  Weirs 1 and 2 are 140 degree, the rest are 120 to 110 degree. 
The following weirs have Panama crests: >3.

Our naming convention for sediment: sed0=sediment far from invert of V.  sed2=sediment at invert of V.
*/
#include "weir_data_Nov2017.h"

double Cd;
int fred;

/* Q_quality flags are as follows:
0- all ok
1- low flow weir box overtopped
2- high-flow weir below depth of first Cd value
3- both 1 and 2
4- water behind high flow weir above top of V
8- high flow weir depth above the highest Cd value, Q unreliable.
*/

if(weir_num==1 || weir_num==2 )
  {
  /* 140-degree ARS crest, full of sediment */
  if(yes_correct)
    {
    Cd=Cd_interp(point_num,HoverT,num_ars140sed2corr,ars140sed2corr_HoverT,ars140sed2corr_Cd,quality_flag);
    }
  else
    {
    Cd=Cd_interp(point_num,HoverT,num_ars140sed2uncorr,ars140sed2uncorr_HoverT,ars140sed2uncorr_Cd,quality_flag);
    }

/*  #########OLD WAY#########
 *   if(HoverT<2.87) Cd=0.495+0.06835*HoverT;  // from lab results 140 degree, full of sediment. 
 *   else
 *     {
 *     Cd=0.495+0.06835*2.87;  //see Jesse's thesis 
 *     quality_flag[point_num]=2;
 *     }
 */ 
  }
/*----------------------------------------------*/
else if(weir_num==3)
  {
  /* 120-degree ARS crest, full of sediment */
  
  if(yes_correct)
    {
    Cd=Cd_interp(point_num,HoverT,num_ars120sed2corr,ars120sed2corr_HoverT,ars120sed2corr_Cd,quality_flag);
    }
  else
    {
    Cd=Cd_interp(point_num,HoverT,num_ars120sed2uncorr,ars120sed2uncorr_HoverT,ars120sed2uncorr_Cd,quality_flag);
    }
  }
/*----------------------------------------------*/
else if(weir_num==4)
  {
  if(yes_correct)
    {
    printf("Can't correct weir No. 4.\n");
    printf("Don't try to use corrected Cd values for this weir.  Program stopped.\n");
    exit(0);
    }
  else
    {
    if(yes_sedimented==FALSE)  Cd=Cd_interp(point_num,HoverT,num_w4sed0uncorr,w4sed0uncorr_HoverT,w4sed0uncorr_Cd,quality_flag);
    else                       Cd=Cd_interp(point_num,HoverT,num_w4sed2uncorr,w4sed2uncorr_HoverT,w4sed2uncorr_Cd,quality_flag);
    }
  }
/*----------------------------------------------*/
else if(weir_num==5)
  {
  if(yes_correct)
    {
    printf("Can't correct weir No. 5.\n");
    printf("Don't try to use corrected Cd values for this weir.  Program stopped.\n");
    exit(0);
    }
  else
    {
    if(yes_sedimented==FALSE)  Cd=Cd_interp(point_num,HoverT,num_w5sed0uncorr,w5sed0uncorr_HoverT,w5sed0uncorr_Cd,quality_flag);
    else                       Cd=Cd_interp(point_num,HoverT,num_w5sed2uncorr,w5sed2uncorr_HoverT,w5sed2uncorr_Cd,quality_flag);
    }
  }
/*----------------------------------------------*/
else if(weir_num==9)
  {
  if(yes_correct)
    {
    printf("Can't correct weir No. 9.\n");
    printf("Don't try to use corrected Cd values for this weir.  Program stopped.\n");
    exit(0);
    }
  else
    {
    if(yes_sedimented==FALSE)  Cd=Cd_interp(point_num,HoverT,num_w9sed0uncorr,w9sed0uncorr_HoverT,w9sed0uncorr_Cd,quality_flag);
    else                       Cd=Cd_interp(point_num,HoverT,num_w9sed2uncorr,w9sed2uncorr_HoverT,w9sed2uncorr_Cd,quality_flag);
    }
  }
/*----------------------------------------------*/
else if(weir_num==10)
  {
  if(yes_correct)
    {
    printf("Can't correct weir No. 10.\n");
    printf("Don't try to use corrected Cd values for this weir.  Program stopped.\n");
    exit(0);
    }
  else
    {
    if(yes_sedimented==FALSE)  Cd=Cd_interp(point_num,HoverT,num_w10sed0uncorr,w10sed0uncorr_HoverT,w10sed0uncorr_Cd,quality_flag);
    else                       Cd=Cd_interp(point_num,HoverT,num_w10sed2uncorr,w10sed2uncorr_HoverT,w10sed2uncorr_Cd,quality_flag);
    }
  }
/*----------------------------------------------*/
else if(weir_num==11)
  {
  if(yes_correct)
    {
    printf("Can't correct weir No. 11.\n");
    printf("Don't try to use corrected Cd values for this weir.  Program stopped.\n");
    exit(0);
    }
  else
    {
    if(yes_sedimented==FALSE)  Cd=Cd_interp(point_num,HoverT,num_w11sed0uncorr,w11sed0uncorr_HoverT,w11sed0uncorr_Cd,quality_flag);
    else                       Cd=Cd_interp(point_num,HoverT,num_w11sed2uncorr,w11sed2uncorr_HoverT,w11sed2uncorr_Cd,quality_flag);
    }
  }
/*----------------------------------------------*/
else if(weir_num==12)
  {
  if(yes_correct)
    {
    printf("Can't correct weir No. 12.\n");
    printf("Don't try to use corrected Cd values for this weir.  Program stopped.\n");
    exit(0);
    }
  else
    {
    if(yes_sedimented==FALSE)  Cd=Cd_interp(point_num,HoverT,num_w12sed0uncorr,w12sed0uncorr_HoverT,w12sed0uncorr_Cd,quality_flag);
    else                       Cd=Cd_interp(point_num,HoverT,num_w12sed2uncorr,w12sed2uncorr_HoverT,w12sed2uncorr_Cd,quality_flag);
    }
  }

/*----------------------------------------------*/
else if(weir_num==13)
  {
  if(yes_correct)
    {
    printf("Can't correct weir No. 13.\n");
    printf("Don't try to use corrected Cd values for this weir.  Program stopped.\n");
    exit(0);
    }
  else
    {
    if(yes_sedimented==FALSE)  Cd=Cd_interp(point_num,HoverT,num_w13sed0uncorr,w13sed0uncorr_HoverT,w13sed0uncorr_Cd,quality_flag);
    else                       Cd=Cd_interp(point_num,HoverT,num_w13sed2uncorr,w13sed2uncorr_HoverT,w13sed2uncorr_Cd,quality_flag);
    }
  }
/*----------------------------------------------*/
else if(weir_num==8)
  {
  if(yes_correct==TRUE)
    {
    if(yes_sedimented==TRUE)
      {
      Cd=Cd_interp(point_num,HoverT,num_pan120sed2corr,pan120sed2corr_HoverT,pan120sed2corr_Cd,quality_flag);
      }
    else
      Cd=Cd_interp(point_num,HoverT,num_pan120sed0corr,pan120sed0corr_HoverT,pan120sed0corr_Cd,quality_flag);
    }
  else /* yes_correct==FALSE */
    {
    if(yes_sedimented==TRUE)
      {
      Cd=Cd_interp(point_num,HoverT,num_pan120sed2uncorr,pan120sed2uncorr_HoverT,pan120sed2uncorr_Cd,quality_flag);
      }
    else
      Cd=Cd_interp(point_num,HoverT,num_pan120sed0uncorr,pan120sed0uncorr_HoverT,pan120sed0uncorr_Cd,quality_flag);
    }
  }
/*----------------------------------------------*/
else if(weir_num==6)
  {
  if(yes_correct)
    {
    printf("Can't correct weir No. 6.\n");
    printf("Don't try to use corrected Cd values for this weir.  Program stopped.\n");
    exit(0);
    }
  else
    {
    if(yes_sedimented==FALSE)  Cd=Cd_interp(point_num,HoverT,num_w6sed0uncorr,w6sed0uncorr_HoverT,w6sed0uncorr_Cd,quality_flag);
    else                       Cd=Cd_interp(point_num,HoverT,num_w6sed2uncorr,w6sed2uncorr_HoverT,w6sed2uncorr_Cd,quality_flag);
    }
  }
/*----------------------------------------------*/
else if(weir_num==7)
  {
  fprintf(stderr,"You called flow_calc on data from Agua Salud weir 7 with the high-flow option. Weir 7 is a low-flow weir\n");
  fprintf(stderr,"Program stopped\n");
  exit(-1);
  }

return(Cd);
}

/*#######################################################################*/
/*################## INTERPOLATE Cd VALUES ##############################*/
/*#######################################################################*/
extern double Cd_interp(int point_num,double HoverT, int num,double *h, double *cd,int *quality_flag)
{
/* Q_quality flags are as follows:
0- all ok
1- low flow weir box overtopped
2- high-flow weir below depth of first Cd value
3- both 1 and 2
4- water behind high flow weir above top of V, not bad thing for weir 4 and 6.  Bad for weir 5, 8, 9, 10, 11.
8- high flow weir depth above the highest Cd value, Q unreliable.
*/

double val;
int i;

if(HoverT<h[0])
  {
  val=cd[0];
  quality_flag[point_num]+=2;  /* high-flow weir below depth of first Cd value */
  }
else if(HoverT>h[num-1])
  {
  val=cd[num-1];
  quality_flag[point_num]+=8;  /* high flow weir depth above the highest Cd value, Q unreliable. */
  }
else
  {
  for(i=1;i<num;i++)
    {
    if(HoverT<h[i])
      {
      /* the value is in this interval */
      val=cd[i-1]+(cd[i]-cd[i-1])*(HoverT-h[i-1])/(h[i]-h[i-1]);
      break;
      }
    }
  }

return(val);
}

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/*#######################################################################*/
/*################## SANITY CHECK AND OPEN FILES ########################*/
/*#######################################################################*/
extern void sanity_check_and_open_files(char *ProgName, int *sitenum,
                            FILE **in_high_fptr, FILE **in_low_fptr, FILE **out_q_fptr,
                            FILE **out_jq_fptr,FILE **out_dq_fptr,FILE **out_jdq_fptr,
                            char *in_high_file_name,char *in_low_file_name,char *out_q_file_name,
                            char *out_jq_file_name,char *out_dq_file_name, char *out_jdq_file_name,
                            int yes_write_jdate_q,int yes_daily,
                            int yes_daily_jdate)
{
char pathname[255];
char first5low[16];
char first5high[16];
int high_sitenum;
int low_sitenum;
int i;

/* perform sanity checks.  Are the required things present?   Are the options consistent? */
if(in_high_file_name==NULL && in_low_file_name==NULL)
  {
  printf("\nYou must specify at least one input file name with the -h or -l option\n");
  printf(" Program stopped.\n\n");
  usage(ProgName);
  }

if(out_q_file_name==NULL && out_jq_file_name==NULL && out_dq_file_name==NULL && out_jdq_file_name==NULL)
  {
  printf("\nYou must specify at least one output file with the -q, -j, -d or -D options\n");
  printf("Program stopped\n\n");
  }
/* everything seems ok with the options, so go ahead and open the files */

high_sitenum=0;  
if(in_high_file_name!= NULL)
  {
  if(((*in_high_fptr)=fopen(in_high_file_name,"r"))==NULL)   
    {
    printf("\nCan't open high-flow weir file %s for reading.\n",in_high_file_name);
    usage(ProgName);
    }
  /* interpret the site number from the input file name */
  for(i=0;i<5;i++)
    {
    first5high[i]=tolower(in_high_file_name[i]);
    }
  high_sitenum=find_site(first5high);
  }

low_sitenum=0;
if(in_low_file_name!= NULL)
  {
  if(((*in_low_fptr)=fopen(in_low_file_name,"r"))==NULL)   
    {
    printf("\nCan't open low-flow weir file %s for reading.\n",in_low_file_name);
    usage(ProgName);
    }
  /* interpret the site number from the input file name */
  for(i=0;i<5;i++)
    {
    first5low[i]=tolower(in_low_file_name[i]);
    }
  low_sitenum=find_site(first5low);
  }
if((in_low_file_name!=NULL && in_high_file_name!=NULL)  && (low_sitenum!=high_sitenum))
  {
  printf("Your high flow and low flow data come from different weirs.  The program\n");
  printf("detected that you entered high flow weir %s, and low flow weir %s.\n",first5high,first5low);
  printf("Program stopped.\n\n");
  exit(-99);
  }

if(out_q_file_name!=NULL)   /* open the output file name MM/DD/YYYY */
  {
  if(((*out_q_fptr)=fopen(out_q_file_name,"w"))==NULL)   
    {
    printf("\nCan't open file %s for writing.\n",out_q_file_name);
    usage(ProgName);
    }
  }
if(out_jq_file_name!=NULL)  /* open the output file JDATE format */
  {
  if(((*out_jq_fptr)=fopen(out_jq_file_name,"w"))==NULL)   
    {
    printf("\nCan't open file %s for writing.\n",out_jq_file_name);
    usage(ProgName);
    }
  }
if(out_dq_file_name!=NULL)   /* open the daily output file MM/DD/YYYY */
  {
  if(((*out_dq_fptr)=fopen(out_dq_file_name,"w"))==NULL)   
    {
    printf("\nCan't open file %s for writing.\n",out_dq_file_name);
    usage(ProgName);
    }
  }
if(out_jdq_file_name!=NULL)  /* open the daily output file in JDATE format */
  {
  if(((*out_jdq_fptr)=fopen(out_jdq_file_name,"w"))==NULL)   
    {
    printf("\nCan't open file %s for writing.\n",out_jdq_file_name);
    usage(ProgName);
    }
  }
if(high_sitenum>0) (*sitenum)=high_sitenum;
else               (*sitenum)=low_sitenum;

return;
}

/*#################################################################################*/
/*################################ READ FILE ######################################*/
/*#################################################################################*/

void read_file(FILE *in_fptr, char *in_file_name, int callnum, int *nlines, 
               int *filetype, int yes_one_val,int yes_temperature,
               double *jdate, double *mm,int *yes_write_new_file,FILE *out_new_fptr)
{
char theString[15001];  /* hopefully this is big enough */
char *tstr=NULL;
int i,num,go,in_data,commacol,len,fred;
long month,day,year,hour,minute,second;
double dsec,dat;
long maxlines;

go=TRUE;
num=0;
i=0;
maxlines=(*nlines);
*nlines=0;


if(callnum==1) /* count the total number of lines in the file and return */
  {
  in_data=0;
  do
    {
    tstr=fgets(theString,15000,in_fptr);
    if(tstr==NULL) go=FALSE;
    if(in_data==1)
      {
      /* let's find out if the filetype is 1 (AM/PM) or 2 (24-hour clock) */
      len=strlen(theString);
      *filetype=2;
      for(i=0;i<len-1;i++)
        {
        if((theString[i]=='A' || theString[i]=='P') && theString[i+1]=='M') *filetype=1;
        }
      in_data=2;
      }
    if(strncmp(theString,"-----",5)==0 && in_data==0)
      {
      in_data=1;
      }
    i++;
    } while(go==TRUE);
  *nlines=i;
  if(in_data==0)
    {
    /* this is bad, this means that the string ----- was not found in the input file */
    *nlines=0;
    }
  if(*nlines==0)
    {
    printf("\nWhile reading file %s\n",in_file_name);
    printf("There is a problem with your input data file.   The string '-----' is expected\n");
    printf("the line before the first line of data: DD/MM/YYYY HH:MM:SS ...\n");
    printf("But- this string was not found in the file.   Check your file format.\n");
    printf("Program stopped.\n");
    exit(0);
    }
  if(*nlines>=REDICULOUSLY_LARGE)
    {
    printf("The number of lines in the file is rediculously large.\n");
    printf("Why is your file so large, or is there some other error?\n");
    printf("program terminated.\n");
    exit(-9);
    }
  return;
  }
in_data=FALSE;
if(callnum==2) /* read each line, parse data lines and store values */
  {
  /* read the header */
  do
    {
    tstr=fgets(theString,15000,in_fptr);
    if(strncmp(theString,"-----",5)==0) in_data=TRUE;
    if(*yes_write_new_file==1) fprintf(out_new_fptr,"%s",theString);
    } while(in_data==FALSE);
  
  if(*yes_write_new_file==1) *yes_write_new_file=2; /* this is CLEVER.  Makes sure header not written again */
                                                    /* note that CLEVER in this context means DANGEROUS */
    
  /* read the data */  
  do
    {
    tstr=fgets(theString,15000,in_fptr);
    if(tstr==NULL || theString[0]=='X' || theString[0]==',') go=FALSE;  /* for some reason, the TS editor writes */
                                                  /* XY Series Name: at a point near the end of the file, or a comma */
    else
      {
      num++;
      len=strlen(theString);
      if(len<15)
        {
        printf("\nThere is a short line in your input file at line number %d.\n",num);
        printf("Check for corrupt data or an extra line-feed.  Program stopped.\n\n");
        exit(0);
        }
      parse_line(theString,*filetype,yes_one_val,yes_temperature,num,&year,&month,&day,&hour,&minute,&second,&dat);
      jdate[num]=greg_2_jul(year,month,day,hour,minute,(double)second);
      mm[num]=dat;
      *nlines=num;
      if(*nlines>maxlines)
        {
        printf("\nProblem reading your input file.  Something unexpected in the file format.\n");
        printf("Look at the file you are trying to process and compare it to a file that\n");
        printf("processed successfully.   You might need to re-format your input file to\n");
        printf("look like a 'good' file.\nThis program expects a line of ------- with at least\n");
        printf("five dashes '-' before the first line of data: MM/DD/YYYY....\n");
        printf("Also- does the file you are trying to process contain temperature data?  If so\n");
        printf("you must specify the -t option in the command line.\n");
        printf("Program terminated.\n\n");
        exit(0);
        }
      }
    } while(go==TRUE);
  }
return;
}

/*####################################################################*/
/*########################### PARSE LINE #############################*/
/*####################################################################*/
void parse_line(char *theString,int filetype,int yes_one_val,int yes_temperature,int lnum,
                long *year,long *month, long *day,long *hour,
                long *minute, long *second,double *dat)
{
char str[20];
long yr,mo,da,hr,mi,se;
double mm,julian;
int i,start,end,len;
int yes_pm,wordlen;
char theWord[15000];

len=strlen(theString);

start=0; /* begin at the beginning of theString */
get_word(theString,&start,&end,theWord,&wordlen);
mo=atol(theWord);

get_word(theString,&start,&end,theWord,&wordlen);
da=atol(theWord);

get_word(theString,&start,&end,theWord,&wordlen);
yr=atol(theWord);

get_word(theString,&start,&end,theWord,&wordlen);
hr=atol(theWord);

get_word(theString,&start,&end,theWord,&wordlen);
mi=atol(theWord);

get_word(theString,&start,&end,theWord,&wordlen);
se=atol(theWord);

/* if filetype==1,or filetype==3 then this file has an AM or a PM in it.  So, read it. */
yes_pm=FALSE;
if(filetype==1 || filetype==3)
  {
  get_word(theString, &start, &end,theWord, &wordlen);
  if(strncmp(theWord,"PM",2)==0)    yes_pm=TRUE;
  else                              yes_pm=FALSE;
  }

if(!yes_one_val)
  {
  /* read in the record number and ignore it. */
  get_word(theString,&start,&end,theWord,&wordlen);
  }
/* if there are temperature data, we must read those.... */
if(yes_temperature==TRUE && filetype<3)  /* don't read temperature data from baro file, which is type 3 or 4 */
  {
  get_word(theString,&start,&end,theWord,&wordlen);
  }

/* read the pressure or depth value */
get_word(theString,&start,&end,theWord,&wordlen);
mm=atof(theWord);

if(yr<11) yr+=2000;   /* some idiot used a two digit year in the 2000's */
if(yr>11 && yr<100) yr+=1900;  /* some idiot used a two digit year back in the 1900's */

*dat=mm;
*year=yr;
*month=mo;
*day=da;
if(filetype==1)
  {
  if(yes_pm==FALSE && hr==12) hr=0;
  if(yes_pm==TRUE  && hr<12) hr+=12;
  }
*hour=hr;
*minute=mi;
*second=se;
  
return;
}

/*####################################################################*/
/*############################## GET WORD ############################*/
/*####################################################################*/
void get_word(char *theString,int *start,int *end,char *theWord,int *wordlen)
{
int i,lenny,j;
lenny=strlen(theString);

while(theString[*start]==' ' || theString[*start]=='\t')
  {
  (*start)++;
  };
  
j=0;
for(i=(*start);i<lenny;i++)
  {
  if(theString[i]!=' ' && theString[i]!='\t' && theString[i]!=',' && theString[i]!=':' && theString[i]!='/')
    {
    theWord[j]=theString[i];
    j++;
    }
  else
    {
    break;
    }
  }
theWord[j]='\0';
*start=i+1;
*wordlen=strlen(theWord);
return;
}

/*#####################################################################*/
/*############################# USAGE #################################*/
/*#####################################################################*/

void usage (char *ProgName)
{
   fprintf (stderr, "usage:  %s [-options ...]\n\n", ProgName);
   fprintf (stderr, "where options include:\n");
   fprintf (stderr, "   -l  <filename> input low-flow weir water level (mm) file <filename>\n");
   fprintf (stderr, "   -h  <filename> input high-flow weir water level (mm) file <filename>\n");
   fprintf (stderr, "   -o  <filename> write flows to the file <filename>, MM/DD/YYYY format\n");
   fprintf (stderr, "   -j  <filename> write flows to the file <filename>, Julian date format\n");
   fprintf (stderr, "   -d  <filename> write daily average Q to file <filename>, MM/DD/YYYY format\n");
   fprintf (stderr, "   -D  <filename> write daily average Q to file <filename>, Julian date format\n");
   fprintf (stderr, "   -s  write both high and low flow series to file, separated by '&'\n");
   fprintf (stderr, "   -c  use Cd values corrrected for velocity head.  NOT RECOMMENDED\n");
   fprintf (stderr, "   -m  flag indicating that depths in the input file(s) are in meters!\n\n");
   fprintf (stderr, "NOTES:\n");
   fprintf (stderr, "1.  The 'best' flow value is only calculated if the -h and -l flags are used.\n");
   fprintf (stderr, "2.  If the depth in the weir box is less than the spill depth, the smallest\n");
   fprintf (stderr, "    of the high- and low-flow weir flows is assumed to be correct.\n");
   fprintf (stderr, "3.  The files specified by either -o or -j contain in the first column\n");
   fprintf (stderr, "    the 'best' flow, followed by the low-flow weir box flow in the second.\n");
   fprintf (stderr, "4.  The daily average output stuff is not yet working- FLO Nov. 28, 2010\n");
   fprintf (stderr, "5.  Weir 7 is special, it has no high flow weir.  Use this program for \n");
   fprintf (stderr, "    that weir like this:  flow_calc -l V07ASH_2012ycJunkTest.csv -j fred.out\n\n");
   
   exit(-1);
}

/*##############################################################################*/
/*######################## MEMORY ALLOCATION ROUTINES ##########################*/
/*##############################################################################*/

void itwo_alloc(int ***array,int rows, int cols)
{
int  i,frows,fcols, numgood=0;
int error=0;

if ((rows==0)||(cols==0))
  {
  printf("Error: Attempting to allocate array of size 0\n");
  exit;
  }

frows=rows+1;  /* added one for FORTRAN numbering */
fcols=cols+1;  /* added one for FORTRAN numbering */

*array=(int **)malloc(frows*sizeof(int *));
if (*array) 
  {
  memset((*array), 0, frows*sizeof(int*));
  for (i=0; i<frows; i++)
    {
    (*array)[i] =(int *)malloc(fcols*sizeof(int ));
    if ((*array)[i] == NULL)
      {
      error = 1;
      numgood = i;
      i = frows;
      }
     else memset((*array)[i], 0, fcols*sizeof(int )); 
     }
   }
return;
}



void dtwo_alloc(double ***array,int rows, int cols)
{
int  i,frows,fcols, numgood=0;
int error=0;

if ((rows==0)||(cols==0))
  {
  printf("Error: Attempting to allocate array of size 0\n");
  exit;
  }

frows=rows+1;  /* added one for FORTRAN numbering */
fcols=cols+1;  /* added one for FORTRAN numbering */

*array=(double **)malloc(frows*sizeof(double *));
if (*array) 
  {
  memset((*array), 0, frows*sizeof(double *));
  for (i=0; i<frows; i++)
    {
    (*array)[i] =(double *)malloc(fcols*sizeof(double ));
    if ((*array)[i] == NULL)
      {
      error = 1;
      numgood = i;
      i = frows;
      }
     else memset((*array)[i], 0, fcols*sizeof(double )); 
     }
   }
return;
}



void d_alloc(double **var,int size)
{
  size++;  /* just for safety */

   *var = (double *)malloc(size * sizeof(double));
   if (*var == NULL)
      {
      printf("Problem allocating memory for array in d_alloc.\n");
      return;
      }
   else memset(*var,0,size*sizeof(double));
   return;
}

void i_alloc(int **var,int size)
{
   size++;  /* just for safety */

   *var = (int *)malloc(size * sizeof(int));
   if (*var == NULL)
      {
      printf("Problem allocating memory in i_alloc\n");
      return; 
      }
   else memset(*var,0,size*sizeof(int));
   return;
}

/*##############################################################################*/
/*######################## CALENDAR FUNCTIONS (Julian) #########################*/
/*##############################################################################*/


/*
 * convert Gregorian days to Julian date
 *
 * Modify as needed for your application.
 *
 * The Julian day starts at noon of the Gregorian day and extends
 * to noon the next Gregorian day.
 *
 */
/*
** Takes a date, and returns a Julian day. A Julian day is the number of
** days since some base date  (in the very distant past).
** Handy for getting date of x number of days after a given Julian date
** (use jdate to get that from the Gregorian date).
** Author: Robert G. Tantzen, translator: Nat Howard
** Translated from the algol original in Collected Algorithms of CACM
** (This and jdate are algorithm 199).
*/


double greg_2_jul(
long year, 
long mon, 
long day, 
long h, 
long mi, 
double se)
{
    long m = mon, d = day, y = year;
    long c, ya, j;
    double seconds = h * 3600.0 + mi * 60 + se;

    if (m > 2)
	m -= 3;
    else {
	m += 9;
	--y;
    }
    c = y / 100L;
    ya = y - (100L * c);
    j = (146097L * c) / 4L + (1461L * ya) / 4L + (153L * m + 2L) / 5L + d + 1721119L;
    if (seconds < 12 * 3600.0) {
	j--;
	seconds += 12.0 * 3600.0;
    }
    else {
	seconds = seconds - 12.0 * 3600.0;
    }
    return (j + (seconds / 3600.0) / 24.0);
}

/* Julian date converter. Takes a julian date (the number of days since
** some distant epoch or other), and returns an int pointer to static space.
** ip[0] = month;
** ip[1] = day of month;
** ip[2] = year (actual year, like 1977, not 77 unless it was  77 a.d.);
** ip[3] = day of week (0->Sunday to 6->Saturday)
** These are Gregorian.
** Copied from Algorithm 199 in Collected algorithms of the CACM
** Author: Robert G. Tantzen, Translator: Nat Howard
**
** Modified by FLO 4/99 to account for nagging round off error 
**
*/
void calc_date(double jd, long *y, long *m, long *d, long *h, long *mi,
               double *sec)
{
    static int ret[4];

    long j;
    double hundredth_second=(double)0.01/((double)24.0*(double)3600.0);
    double tmp; 
    double frac;

    jd+=hundredth_second;  /* account for round-off error */
    j=(long)jd;
    frac = jd - j;

    if (frac >= 0.5) {
	frac = frac - 0.5;
        j++;
    }
    else {
	frac = frac + 0.5;
    }

    ret[3] = (j + 1L) % 7L;
    j -= 1721119L;
    *y = (4L * j - 1L) / 146097L;
    j = 4L * j - 1L - 146097L * *y;
    *d = j / 4L;
    j = (4L * *d + 3L) / 1461L;
    *d = 4L * *d + 3L - 1461L * j;
    *d = (*d + 4L) / 4L;
    *m = (5L * *d - 3L) / 153L;
    *d = 5L * *d - 3 - 153L * *m;
    *d = (*d + 5L) / 5L;
    *y = 100L * *y + j;
    if (*m < 10)
	*m += 3;
    else {
	*m -= 9;
	*y=*y+1; /* Invalid use: *y++. Modified by Tony */
    }

    /* if (*m < 3) *y++; */
    /* incorrectly repeated the above if-else statement. Deleted by Tony.*/

    tmp = 3600.0 * (frac * 24.0);
    *h = (long) (tmp / 3600.0);
    tmp = tmp - *h * 3600.0;
    *mi = (long) (tmp / 60.0);
    *sec = tmp - *mi * 60.0;
}

int dayofweek(double j)
{
    j += 0.5;
    return (int) (j + 1) % 7;
}
