/*##############################################################################

This file contains the results of all the lab studies done so far on 120-degree
ARS and Panama weir crests with/without sediment.  The special results for weirs
4 and 6 include the effect of flow on the flat part of the weir, as it affects 
the Cd assuming that that v-notch weir equation remains valid.

The Cd values corrected for velocity head effects are here for completeness.
However, I don't recommend they be used because of scale effects in our models
tested at different scales, the corrected values don't line up across scales.
The velocity-head corrected Cd's should only be used when the head in the 
prototype is to scale equal to the head in the model, and the total head in the
prototype channel is used to calculate Q.

Need to add 140-degree results when we get them.  -FLO October 4, 2015. */
/* Added ARS 120 and 140 degree results from latest analysis on 10 May 2016. -FLO */
/* updated results for Panama weirs too, based on results in paper draft */


/* Panama weir crest, 120-degree, full of sediment, uncorrected for velocity head */
int num_pan120sed2uncorr=23;
double pan120sed2uncorr_HoverT[23]={0.500,0.750,1.002,1.194,1.410,1.596,1.786,1.981,2.218,2.323,2.421,2.636,2.766,2.937,3.129,3.208,3.436,3.873,4.299,4.845,5.326,5.859,6.072};
double pan120sed2uncorr_Cd[23]=    {0.614,0.632,0.646,0.654,0.662,0.668,0.673,0.678,0.684,0.686,0.688,0.693,0.695,0.698,0.701,0.702,0.706,0.712,0.717,0.724,0.729,0.734,0.736};

/* Panama weir crest, 120-degree, full of sediment, corrected for a particular velocity head */
int num_pan120sed2corr=24;
double pan120sed2corr_HoverT[24]={0.250,0.350,0.500,0.600,0.700,0.800,0.900,1.000,1.100,1.150,1.200,1.400,1.600,1.800,2.000,2.200,2.400,2.600,2.800,3.000,3.200,3.430,5.000,6.000};
double pan120sed2corr_Cd[24]=    {0.580,0.596,0.613,0.622,0.629,0.636,0.642,0.647,0.652,0.654,0.654,0.656,0.658,0.660,0.661,0.663,0.664,0.665,0.666,0.667,0.668,0.669,0.672,0.674};

/* Panama weir crest, 120-degree, not full of sediment, uncorrected for velocity head */
int num_pan120sed0uncorr=24;
double pan120sed0uncorr_HoverT[24]={0.500,0.700,0.800,0.900,1.050,1.200,1.400,1.600,1.800,2.000,2.100,2.200,2.400,2.800,3.000,3.200,3.400,3.600,4.000,4.400,4.800,5.200,5.600,6.000};
double pan120sed0uncorr_Cd[24]=    {0.616,0.633,0.640,0.646,0.654,0.661,0.669,0.676,0.682,0.688,0.691,0.693,0.698,0.707,0.711,0.709,0.707,0.705,0.702,0.699,0.697,0.695,0.692,0.690};

/* Panama weir crest, 120-degree, not full of sediment, corrected for a particular velocity head */
int num_pan120sed0corr=21;
double pan120sed0corr_HoverT[21]={0.250,0.350,0.500,0.600,0.070,0.800,0.900,1.000,1.100,1.150,1.150,1.300,1.500,1.700,2.000,2.300,2.600,2.900,3.200,3.430,6.000};
double pan120sed0corr_Cd[21]=    {0.580,0.596,0.613,0.622,0.525,0.636,0.642,0.647,0.652,0.654,0.654,0.655,0.657,0.659,0.661,0.663,0.665,0.666,0.668,0.669,0.700};

/*########## WEIR 3 IS THIS TYPE ########### */
/* ARS weir crest, 120-degree, full of sediment, uncorrected for velocity head */
int num_ars120sed2uncorr=16;
double ars120sed2uncorr_HoverT[35]={0.500,1.000,1.242,1.430,1.630,1.820,1.997,2.178,2.226,2.359,2.451,2.649,2.795,2.950,3.129,3.192,3.414,2.992,3.449,3.962,4.40,4.985,5.4834,6.028,6.273,3.471,4.266,5.114,6.053,6.997,8.037,8.737,9.773,10.958,12.08};
double ars120sed2uncorr_Cd[35]=    {0.547,0.600,0.618,0.630,0.641,0.651,0.659,0.667,0.667,0.666,0.666,0.665,0.665,0.664,0.664,0.664,0.664,0.664,0.663,0.662,0.661,0.660,0.660,0.659,0.659,0.663,0.662,0.660,0.659,0.658,0.657,0.657,0.656,0.655,0.654};

/* ARS weir crest, 120-degree, full of sediment, corrected for a particular velocity head */
int num_ars120sed2corr=27;
double ars120sed2corr_HoverT[27]={0.330,0.580,0.600,0.650,0.700,0.800,0.900,1.000,1.100,1.200,1.300,1.400,1.500,1.600,1.700,1.900,2.000,2.200,2.400,2.640,2.800,3.000,3.200,3.400,3.430,4.000,6.000};
double ars120sed2corr_Cd[27]=    {0.521,0.555,0.557,0.562,0.567,0.576,0.583,0.590,0.596,0.602,0.608,0.613,0.617,0.622,0.626,0.634,0.637,0.644,0.650,0.657,0.662,0.667,0.672,0.676,0.677,0.660,0.660};

/* ARS weir crest, 120-degree, no sediment, uncorrected for velocity head */
int num_ars120sed0uncorr=21;
double ars120sed0uncorr_HoverT[21]={0.370,0.489,0.592,0.702,1.041,1.222,1.436,1.604,1.799,1.997,2.239,2.347,2.437,2.638,2.780,2.931,2.992,3.133,3.214,3.348,3.497};
double ars120sed0uncorr_Cd[21]=    {0.556,0.573,0.585,0.596,0.623,0.634,0.645,0.653,0.661,0.669,0.677,0.681,0.684,0.690,0.694,0.698,0.699,0.694,0.692,0.687,0.683};

/* ARS weir crest, 120-degree, no sediment, corrected for a particular velocity head */
int num_ars120sed0corr=21;
double ars120sed0corr_HoverT[21]={0.580,0.706,1.045,1.233,1.440,1.619,1.818,1.987,2.225,2.343,2.436,2.631,2.640,3.442,3.990,4.444,5.006,5.534,6.052,6.303,12.000};
double ars120sed0corr_Cd[21]=    {0.576,0.589,0.616,0.627,0.638,0.647,0.655,0.662,0.670,0.674,0.677,0.683,0.683,0.659,0.649,0.644,0.638,0.639,0.643,0.648,0.648};

/*######### WEIRS 1 & 2 ARE THIS TYPE ##########*/
/* ARS weir crest, 140-degree, full of sediment, uncorrected for velocity head */
int num_ars140sed2uncorr=15;
double ars140sed2uncorr_HoverT[15]={0.330,0.500,0.750,1.000,1.200,1.400,1.600,1.800,2.000,2.200,2.400,2.600,2.800,3.000,6.000};
double ars140sed2uncorr_Cd[15]=    {0.610,0.619,0.631,0.643,0.653,0.663,0.672,0.682,0.692,0.702,0.711,0.721,0.731,0.741,0.740};

/* ARS weir crest, 140-degree, full of sediment, corrected for a particular velocity head */
int num_ars140sed2corr=13;
double ars140sed2corr_HoverT[13]={0.500,0.750,1.000,1.200,1.400,1.600,1.800,2.000,2.200,2.400,2.600,2.800,4.000};
double ars140sed2corr_Cd[13]=    {0.623,0.631,0.639,0.645,0.652,0.658,0.665,0.671,0.677,0.684,0.690,0.697,0.697};

/* ARS weir crest, 140-degree, no sediment, uncorrected for velocity head */
int num_ars140sed0uncorr=15;
double ars140sed0uncorr_HoverT[15]={0.330,0.500,0.750,1.000,1.200,1.400,1.600,1.800,2.000,2.200,2.400,2.600,2.800,3.000,6.000};
double ars140sed0uncorr_Cd[15]=    {0.648,0.655,0.666,0.677,0.685,0.694,0.703,0.711,0.720,0.729,0.737,0.746,0.754,0.763,0.763};

/* ARS weir crest, 140-degree, no sediment, corrected for a particular velocity head */
int num_ars140sed0corr=13;
double ars140sed0corr_HoverT[13]={0.500,0.750,1.000,1.200,1.400,1.600,1.800,2.000,2.200,2.400,2.600,2.800,4.000};
double ars140sed0corr_Cd[13]=    {0.662,0.669,0.677,0.683,0.689,0.696,0.702,0.708,0.714,0.720,0.727,0.733,0.733};
/*#################################################*/
/*#################################################*/
/*#################################################*/


/* weir4 with no sediment, compound weir, uncorrected */
int num_w4sed0uncorr=16;
double w4sed0uncorr_HoverT[16]={0.1956,0.4130,0.7536,1.6231,2.2246,2.8043,3.1333,3.4572,3.6795,3.7964,4.2569,4.5494,4.7941,5.0301,5.3343,5.6831};
double w4sed0uncorr_Cd[16]=    {0.559 ,0.603 ,0.645 ,0.679 ,0.694 ,0.707 ,0.721 ,0.749 ,0.790 ,0.812 ,0.881 ,0.904 ,0.927 ,0.929 ,0.931 ,0.933 };

/* weir4 with sediment, compound weir, uncorrected */
int num_w4sed2uncorr=18;
double w4sed2uncorr_HoverT[18]={0.2094,0.5024,0.9725,1.6318,2.0800,2.3992,2.7264,2.9189,3.0820,3.2362,3.3868,3.5623,3.7256,4.0030,4.3020,4.6584,4.9813,5.2340};
double w4sed2uncorr_Cd[18]=    {0.559 ,0.608 ,0.652 ,0.670 ,0.682 ,0.684 ,0.689 ,0.707 ,0.747 ,0.789 ,0.828 ,0.856 ,0.885 ,0.961 ,1.038 ,1.105 ,1.144 ,1.163 };

/* weir 5 with no sediment, compound weir, uncorrected, based on results from lab study, plus flat portion of w6 */
int num_w5sed0uncorr=21;
double w5sed0uncorr_HoverT[21]={0.1956,0.4130,0.7536,1.623,2.225,2.804,3.217,3.638,4.442,5.051,5.406,6.065,6.232,7.185,7.327,7.751,7.965,8.156,8.413,8.615,8.711};
double w5sed0uncorr_Cd[21]=    {0.559 ,0.603 ,0.645 ,0.679,0.694,0.707,0.711,0.709,0.706,0.701,0.689,0.689,0.693,0.693,0.693,0.694,0.696,0.699,0.704,0.706,0.706};

/* weir 5 with full sediment, compound weir, uncorrected, based on results from lab study, plus flat portion of w6 */
int num_w5sed2uncorr=21;
double w5sed2uncorr_HoverT[21]={0.2094,0.5024,0.9725,1.6318,2.1507,2.8345,3.5000,4.3485,5.2765,6.119,6.3976,6.6437,6.8898,7.1850,7.3268,7.7506,7.9653,8.1562,8.4132,8.6153,8.7106};
double w5sed2uncorr_Cd[21]=    {0.559 ,0.608 ,0.652 ,0.67  ,0.684 ,0.699 ,0.705 ,0.714 ,0.724 ,0.735,0.733 ,0.740 ,0.745 ,0.749 ,0.751 ,0.758 ,0.762 ,0.768 ,0.776 ,0.781 ,0.782};

/* weir6 with no sediment, compound weir, uncorrected */
int num_w6sed0uncorr=16;
double w6sed0uncorr_HoverT[16]={0.1956,0.4130,0.7536,1.6231,2.2246,2.8043,3.2173,3.6376,4.4420,4.5276,4.9377,5.1455,5.3302,5.5789,5.7744,6.1540};
double w6sed0uncorr_Cd[16]=    {0.559 ,0.603 ,0.645 ,0.679 ,0.694 ,0.707 ,0.711 ,0.709 ,0.706 ,0.707 ,0.710 ,0.720 ,0.735 ,0.755 ,0.760 ,0.776 };

/* weir6 with sediment, compound weir, uncorrected */
int num_w6sed2uncorr=13;
double w6sed2uncorr_HoverT[13]={0.2094,0.5024,0.9725,1.6318,2.1507,2.8345,3.5000,4.3485,4.5276,4.6424,5.6089,5.7480,6.0225};
double w6sed2uncorr_Cd[13]=    {0.559 ,0.608 ,0.652 ,0.670 ,0.684 ,0.699 ,0.705 ,0.714 ,0.724 ,0.725 ,0.757 ,0.778 ,0.781};

/* weir 9 with no sediment, compound weir, uncorrected, based on results from weir 6 */
int num_w9sed0uncorr=19;
double w9sed0uncorr_HoverT[19]={0.1956,0.413 ,0.7536,1.6231,2.2246,2.8043,3.2173,3.6376,4.442,5.0507,5.4057,6.0652,6.2318,6.1143,6.3186,7.0714,7.1230,7.2086,7.4429};
double w9sed0uncorr_Cd[19]=    {0.559 ,0.603 ,0.645 ,0.679 ,0.694 ,0.707 ,0.711 ,0.709 ,0.706,0.701 ,0.698 ,0.689 , 0.693, 0.693,0.693 ,0.700 ,0.701 ,0.703 ,0.706 };

/* weir 9 with sediment, compound weir, uncorrected, based on results from weir 6 */
int num_w9sed2uncorr=15;
double w9sed2uncorr_HoverT[15]={0.2094,0.5024,0.9725,1.6318,2.1507,2.8345,3.5000,4.3485,5.2765,6.1190,6.3186,7.0714,7.1230,7.2086,7.4429};
double w9sed2uncorr_Cd[15]=    {0.559 ,0.608 ,0.652 ,0.670 ,0.684 ,0.699 ,0.705 ,0.714 ,0.724 ,0.735 ,0.741 ,0.758 ,0.759 ,0.762 ,0.769 };


/* weir 10 with no sediment, compound weir, uncorrected, based on results from weir 6 */
int num_w10sed0uncorr=13;
double w10sed0uncorr_HoverT[13]={0.1956,0.413,0.7536,1.6231,2.2246,2.8043,3.2173,3.6376,4.442,4.7966,5.0507,5.4057,5.7316};
double w10sed0uncorr_Cd[13]=    {0.559 ,0.603,0.645 ,0.679 ,0.694 ,0.707 ,0.711 ,0.709 ,0.706,0.703 ,0.701 ,0.693 ,0.699 };

/* weir 10 with sediment, compound weir, uncorrected, based on results from weir 6 */
int num_w10sed2uncorr=13;
double w10sed2uncorr_HoverT[13]={0.2094,0.5024,0.9725,1.6318,2.1507,2.8345,3.5000,4.3485,4.7933,5.0507,5.2765,5.4057,5.7316};
double w10sed2uncorr_Cd[13]=    {0.559 ,0.608 ,0.652 ,0.67  ,0.684 ,0.699 ,0.705 ,0.714 ,0.719 ,0.722 ,0.726 ,0.733 , 0.740};

/* weir 11 with no sediment, compound weir, uncorrected, based on results from weir 6 */
int num_w11sed0uncorr=13;
double w11sed0uncorr_HoverT[13]={0.2021,0.4268,0.7788,1.677,2.299,2.898,3.325,3.548,3.759,4.537,4.591,4.679,5.025};
double w11sed0uncorr_Cd[13]=    {0.559 ,0.603 ,0.645 ,0.679,0.694,0.707,0.711,0.709,0.707,0.745,0.751,0.763,0.789};

/* weir 11 with full sediment, compound weir, uncorrected, based on results from weir 6 */
int num_w11sed2uncorr=13;
double w11sed2uncorr_HoverT[13]={0.2094,0.5024,0.9725,1.6318,2.1507,2.8345,3.4333,3.5000,3.9243,4.3485,4.6052,4.8619};
double w11sed2uncorr_Cd[13]=    {0.559 ,0.608 ,0.652 ,0.670 ,0.684 ,0.699 ,0.704 ,0.705 ,0.712 ,0.748 ,0.778 ,0.793 };

/* weir 12 with no sediment, compound weir, uncorrected, based on results from weir 6 */
int num_w12sed0uncorr=13;
double w12sed0uncorr_HoverT[13]={0.2021,0.4268,0.7788,1.677,2.299,2.898,3.325,3.548,3.759,4.537,4.591,4.679,5.025};
double w12sed0uncorr_Cd[13]=    {0.559 ,0.603 ,0.645 ,0.679,0.694,0.707,0.711,0.709,0.707,0.745,0.751,0.763,0.789};

/* weir 12 with full sediment, compound weir, uncorrected, based on results from weir 6 */
int num_w12sed2uncorr=13;
double w12sed2uncorr_HoverT[13]={0.2094,0.5024,0.9725,1.6318,2.1507,2.8345,3.4333,3.5000,3.9243,4.3485,4.6052,4.8619};
double w12sed2uncorr_Cd[13]=    {0.559 ,0.608 ,0.652 ,0.670 ,0.684 ,0.699 ,0.704 ,0.705 ,0.712 ,0.748 ,0.778 ,0.793 };

/* weir 13 with no sediment, compound weir, uncorrected, based on results from weir 6 */
int num_w13sed0uncorr=13;
double w13sed0uncorr_HoverT[13]={0.2021,0.4268,0.7788,1.677,2.299,2.898,3.325,3.548,3.759,4.537,4.591,4.679,5.025};
double w13sed0uncorr_Cd[13]=    {0.559 ,0.603 ,0.645 ,0.679,0.694,0.707,0.711,0.709,0.707,0.745,0.751,0.763,0.789};

/* weir 13 with full sediment, compound weir, uncorrected, based on results from weir 6 */
int num_w13sed2uncorr=13;
double w13sed2uncorr_HoverT[13]={0.2094,0.5024,0.9725,1.6318,2.1507,2.8345,3.4333,3.5000,3.9243,4.3485,4.6052,4.8619};
double w13sed2uncorr_Cd[13]=    {0.559 ,0.608 ,0.652 ,0.670 ,0.684 ,0.699 ,0.704 ,0.705 ,0.712 ,0.748 ,0.778 ,0.793 };


