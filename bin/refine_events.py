#!/usr/bin/env python3

'''Purge and merge events.'''

# Modules
from pathlib import Path
from datetime import timedelta as td
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tools.figures import get_figure, write_figure
from matplotlib.ticker import FormatStrFormatter
from matplotlib.ticker import MultipleLocator

# Use package style sheet
style_sheet = Path(__file__).parents[0] / 'tools/matplotlibrc'
style_sheet = style_sheet.resolve()
plt.style.use(str(style_sheet))

# Ignore events
_minlen = td(hours=6)
_lb = 0.05
_radius = td(hours=3)

# Smooth time-series
def smooth_signal(ts, sp):
	df = pd.DataFrame()
	df['value'] = ts
	df['rvalue'] = df['value'].values[::-1]
	df['backward'] = df['rvalue'].ewm(
		span=sp, adjust=False).mean().values[::-1]
	df['forward'] = df['value'].ewm(span=sp, adjust=False).mean()
	return df[['backward', 'forward']].mean(axis=1)

# List events
def list_events(df):
	# Empty DataFrame
	el = pd.DataFrame()

	# Copy original DataFrame
	df2 = df.copy().reset_index()

	# Identify event starts
	df2['start'] = False
	df2['shift'] = df2['event'].shift(1).fillna(False)
	df2.loc[df2['event'] & ~df2['shift'], 'start'] = True
	el['start'] = df2.loc[df2.start, 'datetime'].values

	# Identify event stops
	df2['stop'] = False
	df2.loc[:, 'shift'] = (
		df2['event'].shift(-1).fillna(False))
	df2.loc[df2['event'] & ~df2['shift'], 'stop'] = True
	el['stop'] = df2.loc[df2.stop, 'datetime'].values

	# Compute durations
	el['duration'] = el['stop'].sub(el['start'])

	return el

# Main
if __name__ == '__main__':
	# Set document root
	root = Path(__file__).parents[1].resolve()

	# Input data
	dfile = root / 'data' / 'V02FORH_events.pickle'

	# Output data
	ofile = root / 'data' / 'V02FORH_refined.pickle'

	# Load data
	df = pd.read_pickle(dfile)

	# List events
	el = list_events(df)
	initial = len(el.index)

	# Purge short events
	purge = el.loc[el.duration <= _minlen, :]
	for e in purge.itertuples():
		df.loc[e.start:e.stop, 'event'] = False
	el = el.loc[el.duration > _minlen, :]

	# Find peaks
	peaks = []
	for e in el.itertuples():
		# Find peak
		pk_time = df.loc[e.start:e.stop, 'value'].idxmax()
		peaks.append(pk_time)

	# Record time of peaks
	el['TOP'] = peaks

	# Compute time to baseflow
	el['TTB'] = el['stop'].sub(el['TOP'])

	# Purge short recessions
	_limit = el['TTB'].quantile(_lb)
	purge = el.loc[el.TTB <= _limit, :]
	for e in purge.itertuples():
		df.loc[e.start:e.stop, 'event'] = False
	el = el.loc[el.TTB > _limit, :]

	# Compute time to peak
	el['TTP'] = el['TOP'].sub(el['start'])

	# Purge short rises
	_limit = el['TTP'].quantile(_lb)
	purge = el.loc[el.TTP <= _limit, :]
	for e in purge.itertuples():
		df.loc[e.start:e.stop, 'event'] = False
	el = el.loc[el.TTP > _limit, :]

	# Report final number of events
	final = len(el.index)
	diff = initial - final
	print('Purged {:d}, Final {:d}'.format(diff, final))

	# Clear old events
	df.loc[:, 'event'] = False

	# Adjust start times
	for e in el.itertuples():
		# Set search window
		_left = e.start - _radius
		_right = e.start + _radius

		# Find minimum
		_min = df.loc[_left:_right, 'value'].idxmin()

		# Reset events
		df.loc[_min:e.stop, 'event'] = True

	# Re-list events
	el = list_events(df)

	# Merge close events
	el['next'] = el['start'].shift(-1)
	el['dry'] = el['next'].sub(el['stop'])
	el2 = el.loc[el.dry <= _radius]
	for e in el2.itertuples():
		df.loc[e.start:e.next, 'event'] = True

	# Re-list events
	el = list_events(df)
	_final = len(el.index)

	# Report
	print('{} events merged'.format(final - _final))

	# Generate event signal
	df['event_signal'] = np.nan
	df.loc[df['event'], 'event_signal'] = df.loc[df['event'], 'value']

	# Compute better y-limits for plotting
	ymin = df['value'].quantile(0.05) * 0.95
	ymax = df['value'].max() * 1.05

	# Plot options
	label = f'event_detection'
	caption = r'Resulting storm event depths after signal decomposition. We removed events less than 6-hours in total duration and events with rise or recession durations in the lower 5\% of all events. We merged periods of baseflow recession less than 3-hours into surrounding events and selected local minima within 3-hours of event initiations as event start times.'

	# Get LaTex figure
	fig, ax = get_figure(size='large')

	# Plot
	df[['value']].plot(ax=ax, logy=True)
	df[['event_signal']].plot(ax=ax, linestyle='--')
	df.loc[df.locked, ['value']].plot(ax=ax, linestyle='', 
		marker='o', color='C4', markeredgecolor='black')
	ax.grid(True, which='both', axis='y')
	ax.yaxis.set_major_formatter(
		FormatStrFormatter('%.1f'))
	ax.set_ylim(ymin, ymax)
	ax.set_xlabel('Datetime (UTC)')
	ax.set_ylabel('Depth behind Weir (mm$H_2O$)')
	ax.legend(['Recorded Sensor Depth', 'Event Stages', 
		'Field Observed Depth'])

	# Format plot and save
	write_figure(fig, label, caption)

	# Save event data
	df.to_pickle(ofile)
