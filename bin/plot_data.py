#!/usr/bin/env python3

'''Plot raw data.'''

# Modules
from pathlib import Path
from datetime import datetime as dt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tools.figures import get_figure, write_figure
from matplotlib.ticker import FormatStrFormatter
from matplotlib.ticker import MultipleLocator

# Use package style sheet
style_sheet = Path(__file__).parents[0] / 'tools/matplotlibrc'
style_sheet = style_sheet.resolve()
plt.style.use(str(style_sheet))

# Main
if __name__ == '__main__':
	# Targets
	start = dt(2016, 4, 20)
	end = dt(2017, 5, 25)

	# Set document root
	root = Path(__file__).parents[1].resolve()

	# Input data
	dfile = root / 'data' / 'V02FORH_DF.pickle'
	lfile = root / 'data' / 'V02FORH_locked.pickle'

	# Output data
	ofile = root / 'data' / 'V02FORH_raw.pickle'

	# Load data
	df = pd.read_pickle(dfile)
	lf = pd.read_pickle(lfile)

	# Trim data
	df = df.loc[start:end, :]
	lf = lf.loc[lf.index >= start, :]
	lf = lf.loc[lf.index <= end, :]

	# Record locked point
	df['locked'] = False
	df.loc[lf.index, 'locked'] = True

	# Save trimmed data
	df.to_pickle(ofile)

	# Compute better y-limits for plotting
	ymin = df['value'].quantile(0.05) * 0.95
	ymax = df['value'].max() * 1.05

	# Plot options
	label = f'raw_data'
	caption = r'Example of an uncorrected record of depth or stage in mm of water behind a weir with typical erroneous measurements indicated by \textbf{A-D}. \textbf{A)} Sudden decrease in stage after a storm event \textbf{B)} Sudden decrease in stage following a field measurement \textbf{C)} Sudden increase in stage not associated with a storm event \textbf{D)} Gradual increase in stage starting in early March and ending at a field observation.'

	# Get LaTex figure
	fig, ax = get_figure(size='large')

	# Top plot
	df[['value']].plot(ax=ax, logy=True)
	df.loc[df.locked, ['value']].plot(ax=ax, linestyle='', 
		marker='o', color='C4', markeredgecolor='black')
	ax.grid(True, which='both', axis='y')
	ax.yaxis.set_major_formatter(
		FormatStrFormatter('%.1f'))
	ax.set_ylim(ymin, ymax)
	ax.set_xlabel('Datetime (UTC)')
	ax.set_ylabel('Depth behind Weir (mm$H_2O$)')
	ax.legend(['Recorded Sensor Depth', 'Field Observed Depth'])

	# Note errors
	plt.annotate('A', (0.32, 0.39), xycoords='figure fraction')
	plt.annotate('B', (0.63, 0.52), xycoords='figure fraction')
	plt.annotate('C', (0.675, 0.56), xycoords='figure fraction')
	plt.annotate('D', (0.805, 0.42), xycoords='figure fraction')

	# Format plot and save
	write_figure(fig, label, caption)
