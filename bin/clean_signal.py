#!/usr/bin/env python3

'''Correct baseflow parameters.'''

# Modules
from pathlib import Path
import pandas as pd
import numpy as np

# Rolling mean
def rolling_mean(ts, win):
	df = pd.DataFrame()
	df['value'] = ts
	df['rvalue'] = df['value'].values[::-1]
	df['backward'] = df['rvalue'].rolling(
		win).mean().values[::-1]
	df['forward'] = df['value'].rolling(win).mean()
	return df[['backward', 'forward']].mean(axis=1)

# Main
if __name__ == '__main__':
	# Set document root
	root = Path(__file__).parents[1].resolve()

	# Input data
	rfile = root / 'data' / 'V02FORH_RO.pickle'
	bfile = root / 'data' / 'V02FORH_BF.pickle'

	# Load data
	rl = pd.read_pickle(rfile)

	# Clean runoff parameters
		# Eliminate non-physical parameters
	rl.loc[rl.m <= 0.0, 'm'] = np.nan

	# Eliminate outliers (runoff)
	hi = rl['m'].quantile(0.975)
	rl.loc[rl.m > hi, 'm'] = np.nan
	lo = rl['m'].quantile(0.025)
	rl.loc[rl.m < lo, 'm'] = np.nan

	# Interpolate parameters (runoff)
	rl.loc[:, 'm'] = rl['m'].interpolate().ffill().bfill()

	# Smooth parameters (runoff)
	rl.loc[:, 'm'] = rolling_mean(rl['m'], 5)

	# Load data
	bl = pd.read_pickle(bfile)

	# Clean baseflow parameters
  		# Eliminate non-physical parameters
	bl.loc[bl.m <= 0.0, 'm'] = np.nan

	# Eliminate outliers (baseflow)
	hi = bl['m'].quantile(0.975)
	bl.loc[bl.m > hi, 'm'] = np.nan
	lo = bl['m'].quantile(0.025)
	bl.loc[bl.m < lo, 'm'] = np.nan

	# Interpolate parameters (baseflow)
	bl.loc[:, 'm'] = bl['m'].interpolate().ffill().bfill()

	# Smooth parameters (baseflow)
	bl.loc[:, 'm'] = rolling_mean(bl['m'], 5)

	# Save event lists
	ofile = root / 'data' / 'V02FORH_BFC.pickle'
	bl.to_pickle(ofile)
	ofile = root / 'data' / 'V02FORH_ROC.pickle'
	rl.to_pickle(ofile)
