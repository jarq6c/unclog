#!/usr/bin/env python3

'''Compute flashiness indices.'''

# Modules
from pathlib import Path
import pandas as pd
import numpy as np
from scipy.stats import variation
import matplotlib.pyplot as plt
from tools.figures import get_figure, write_figure
from tools.tables import write_table

# Use package style sheet
style_sheet = Path(__file__).parents[0] / 'tools/matplotlibrc'
style_sheet = style_sheet.resolve()
plt.style.use(str(style_sheet))

# Round plot limit to nice value
def ceil_base(num, base=10):
	factor = np.ceil(num / base)
	return factor * base

# Round plot limit to nice value (base 10)
def floor10(num):

	magnitude = np.floor(np.log10(num))
	return 10.0 ** magnitude

# Round plot limit to nice value (base 10)
def ceil10(num):
	magnitude = np.ceil(np.log10(num))
	return 10.0 ** magnitude

# Coefficient of variation of 5th exceedances
fifths = np.arange(0.05, 1.0, 0.05)
def CVLF5(x):
	flows = []
	for n in fifths:
		flows.append(x.quantile(n))
	return variation(flows)

# Flashiness indices
f_index = ({
	r'$F_{10/90}$' : lambda x: x.quantile(0.9) / x.quantile(0.1),
	'$F_{20/80}$' : lambda x: x.quantile(0.8) / x.quantile(0.2),
	'$F_{25/75}$' : lambda x: x.quantile(0.75) / x.quantile(0.25),
	'$F_{.5}$' : lambda x: ((x.quantile(0.75) - x.quantile(0.25)) / 
		x.quantile(0.5)),
	'$F_{.6}$' : lambda x: ((x.quantile(0.8) - x.quantile(0.2)) / 
		x.quantile(0.5)),
	'$F_{.8}$' : lambda x: ((x.quantile(0.9) - x.quantile(0.1)) / 
		x.quantile(0.5)),
	'CVLF5' : CVLF5
	})

# Previous flashiness indices
HIS = ({
	r'$F_{10/90}$' : 20.1,
	'$F_{20/80}$' : 6.9,
	'$F_{25/75}$' : 5.2,
	'$F_{.5}$' : 1.5,
	'$F_{.6}$' : 1.8,
	'$F_{.8}$' : 3.5,
	'CVLF5' : 1.56
	})

# Main
if __name__ == '__main__':
	# Set document root
	root = Path(__file__).parents[1].resolve()

	# Input data
	dfile = root / 'data' / 'V02ASHDECFOR_RUNOFF.pickle'
	mfile = root / 'data' / 'V02ASHMANFOR_RUNOFF.pickle'
	raw_file = root / 'data' / 'V02ASHRAWFOR_RUNOFF.pickle'

	# Load data
	df = pd.read_pickle(dfile)
	mf = pd.read_pickle(mfile)
	rf = pd.read_pickle(raw_file)

	# Trim manual data
	start = df.index.min()
	stop = df.index.max()
	mf = mf.loc[start:stop, :]
	rf = rf.loc[start:stop, :]

	# Combine data
	final = df[['runoff_mm']]
	final = final.rename(columns={'runoff_mm': 'TSD'})
	final['MAN'] = mf['runoff_mm']
	final['RAW'] = rf['runoff_mm']

	# Replace poor values
	final = final.fillna(0.0)
	min_val = final.loc[final["RAW"] > 0.0, "RAW"].min()
	final.loc[final["RAW"] == 0.0, "RAW"] = 0.1 * min_val

	# Resample
	final = final.resample('D').sum()

	# Drop last day -- incomplete
	final = final.iloc[:-1]

	# Generate FDC
	N = float(final['TSD'].count())
	fdc = pd.DataFrame(index=np.arange(1, int(N+1)))
	fdc['TSD'] = final.sort_values('TSD', ascending=False)['TSD'].values
	fdc['MAN'] = final.sort_values('MAN', ascending=False)['MAN'].values
	fdc['RAW'] = final.sort_values('RAW', ascending=False)['RAW'].values

	# Exceedance probability
	#   Weibull plotting position (Bedient and Huber, 1988)
	fdc['p'] = fdc.index / (N+1.0)

	# Compute differences
	fdc['res_TSD'] = fdc['TSD'].div(fdc['RAW'])
	fdc['res_MAN'] = fdc['MAN'].div(fdc['RAW'])

	# Print total flows
	print('Total flow (TSD): {:.3f}'.format(fdc['TSD'].sum()))
	print('Total flow (MAN): {:.3f}'.format(fdc['MAN'].sum()))
	print('Total flow (RAW): {:.3f}'.format(fdc['RAW'].sum()))

	# Compute flashiness indices
	flashiness = pd.DataFrame(index=f_index.keys())
	flashiness['TSD'] = 0.0
	flashiness['MAN'] = 0.0
	flashiness['HIS'] = 0.0
	flashiness['RAW'] = 0.0
	for i in f_index:
		flashiness.loc[i, 'TSD'] = f_index[i](fdc['TSD'])
		flashiness.loc[i, 'MAN'] = f_index[i](fdc['MAN'])
		flashiness.loc[i, 'RAW'] = f_index[i](fdc['RAW'])
		flashiness.loc[i, 'HIS'] = HIS[i]

	# Plot options
	label = f'fdc'
	caption = r'Daily runoff flow duration curves derived from automatically corrected (TSD), manually corrected (MAN), and unprocessed (RAW) stage data. Period: 2016 April 20 to 2017 May 25 not including days with uncorrected negative discharge values.'

	# Get LaTex figure
	# fig, (tx, bx) = get_figure(size='large', nrows=2, ncols=1, sharex=True, 
	# 	gridspec_kw={'hspace' : 0.0, 'height_ratios' : [1, 4]})
	fig, bx = get_figure(size="large wide")

	bx.semilogy(fdc.p, fdc.RAW, label='RAW', color='C10')
	bx.semilogy(fdc.p, fdc.TSD, label='TSD', color='C5')
	bx.semilogy(fdc.p, fdc.MAN, label='MAN', color='C9')

	# tx.semilogy(fdc.p, fdc.res_TSD, label='TSD', color="C5")
	# tx.semilogy(fdc.p, fdc.res_MAN, label='MAN', color="C9")

	# tx.tick_params(axis='y', which='both', direction='in', 
	# 	right=True)
	# tx.tick_params(axis='x', which='both', direction='in')
	# tx.grid(True, which='major', axis='y')
	# tx.grid(True, which='major', axis='x')

	bx.tick_params(axis='y', which='both', direction='in', 
		right=True)
	bx.tick_params(axis='x', which='both', direction='in')
	bx.grid(True, which='both', axis='y')
	bx.grid(True, which='major', axis='x')

	ylo = floor10(final[["TSD", "MAN", "RAW"]].min().min())
	yhi = ceil10(final[["TSD", "MAN", "RAW"]].max().max())

	bx.set_ylim(10e-4, yhi)
	bx.set_xlim(0.0, 1.0)
	# tx.set_yticks([1.0, 1.0, 1.25, 1.5])

	bx.set_xlabel('Exceedance Probability')
	# tx.yaxis.set_label_position('right')
	# tx.yaxis.tick_right()
	bx.set_ylabel('Runoff ($mm d^{-1}$)')
	# tx.set_ylabel(r'$\frac{TSD}{MAN}$', rotation=0, labelpad=20)

	bx.legend()

	# Format plot and save
	write_figure(fig, label, caption)

	# Plot options
	label = f'flashiness'
	caption = r'Runoff flashiness indices derived from automatically corrected (TSD), manually corrected (MAN), historical (HIS), and raw (RAW) stage data.'

	# Get LaTex figure
	fig, ax = get_figure(size='large')
	flashiness.plot.bar(ax=ax, rot=0, zorder=3, edgecolor='black', 
		color=['C5', 'C9', 'C6', 'C10'])

	ax.tick_params(axis='y', which='both', direction='in', 
		right=True)
	ax.tick_params(axis='x', which='major', direction='in', 
		top=True)
	ax.tick_params(axis='x', which='minor', bottom=False)
	ax.grid(False)
	ax.grid(True, which='major', axis='y', zorder=0)

	ax.set_xlabel('Flashiness Index')

	ax.set_ylim(0.0, 25)

	ax.legend()

	# Format plot and save
	write_figure(fig, label, caption)

	# Write table
	label = r'flash'
	caption = r'Runoff flashiness indices derived from automatically corrected (TSD), manually corrected (MAN), historical (HIS), and raw (RAW) stage data.'
	
	# Write table
	flashiness.index.name = "Index"
	flashiness = flashiness.reset_index()
	write_table(flashiness, label, caption, escape=False)
