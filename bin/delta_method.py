#!/usr/bin/env python3

'''Plot example event detection using delta method.'''

# Modules
from pathlib import Path
from datetime import datetime as dt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tools.figures import get_figure, write_figure
from matplotlib.ticker import FormatStrFormatter
from matplotlib.ticker import MultipleLocator

# Use package style sheet
style_sheet = Path(__file__).parents[0] / 'tools/matplotlibrc'
style_sheet = style_sheet.resolve()
plt.style.use(str(style_sheet))

# Main
if __name__ == '__main__':
	# Set document root
	root = Path(__file__).parents[1].resolve()

	# Input data
	ifile = root / 'data' / 'V02FORH_raw.pickle'

	# Load data
	df = pd.read_pickle(ifile)

	# Moving averages
	df['forward'] = df['value'].rolling('12H').mean()
	df['backward'] = df['value'].values[::-1]
	df.loc[:, 'backward'] = df['backward'].rolling('12H').mean().values[::-1]

	# Delta
	df['delta'] = df['backward'].sub(df['forward'])
	df['slope'] = df['delta'].diff()
	df['Zero-line'] = 0.0

	# Normalize for plotting
	df = df.loc[dt(2016, 7, 29):dt(2016, 8, 7), :]
	divisor = max(df['delta'].max(), np.abs(df['delta'].min())) * 1.05
	df.loc[:, 'delta'] = df['delta'].div(divisor)

	# Rename columns
	df = df.rename(columns=({'value': 'Original', 
		'forward': 'Forward MA', 'backward': 'Backward MA', 
		'delta': 'Delta', 'slope': 'Delta-slope'}))

	# Plot options
	label = f'delta_method'
	caption = r'Comparison of forward and backward moving averages for event detection. The difference between moving averages (\emph{backward - forward}) results in a \emph{delta} that when non-zero indicates the presence of an event measurement. This method used the sign and slope of \emph{delta} to indicate the beginning and end of individual events.'

	# Get LaTex figure
	fig, (tx, bx) = get_figure(size='large', nrows=2, ncols=1, sharex=True, 
		gridspec_kw={'hspace' : 0.1, 'height_ratios' : [4, 1]})

	# Top plot
	df[['Original', 'Forward MA', 'Backward MA']].plot(ax=tx)
	tx.set_xlim(dt(2016, 7, 29), dt(2016, 8, 7))
	tx.set_ylim(75.0, 225.0)
	tx.yaxis.set_major_locator(MultipleLocator(25.0))
	tx.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
	tx.yaxis.set_minor_locator(MultipleLocator(5.0))
	tx.set_ylabel('Depth behind weir (mm $H_2O$)')
	tx.grid(False)

	# Bottom plot
	df[['Zero-line']].plot(ax=bx, style='--k')
	df[['Delta', 'Delta-slope']].plot(ax=bx)
	bx.set_ylim(-1.0, 1.0)
	bx.yaxis.set_major_locator(MultipleLocator(1.0))
	bx.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
	bx.yaxis.set_minor_locator(MultipleLocator(0.25))
	bx.legend(loc='right')
	bx.set_xlabel('Datetime (UTC)')
	bx.set_ylabel('Normalized\nDifferences')
	bx.grid(False)

	# Format plot and save
	write_figure(fig, label, caption)
