'''Convert stage to discharge'''

# Modules
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tools.figures import get_figure, write_figure

# Use package style sheet
style_sheet = Path(__file__).parents[0] / 'tools/matplotlibrc'
style_sheet = style_sheet.resolve()
plt.style.use(str(style_sheet))

if __name__ == '__main__':
	# Set document root
	root = Path(__file__).parents[1].resolve()

	# Input data
	dfile = root / 'data' / 'V02ASHDECFOR_RUNOFF.pickle'
	mfile = root / 'data' / 'V02ASHMANFOR_RUNOFF.pickle'
	raw_file = root / 'data' / 'V02ASHRAWFOR_RUNOFF.pickle'

	# Load data
	df = pd.read_pickle(dfile)
	mf = pd.read_pickle(mfile)
	rf = pd.read_pickle(raw_file)

	# Limit precision
	df = df.round(4)
	mf = mf.round(4)
	rf = rf.round(4)

	# Trim manual data
	start = df.index.min()
	stop = df.index.max()
	mf = mf.loc[start:stop, :]
	rf = rf.loc[start:stop, :]

	# Combine data
	all_data = df[['discharge_cms']]
	all_data = all_data.rename(columns={'discharge_cms': 'TSD'})
	all_data['MAN'] = mf['discharge_cms']
	all_data['RAW'] = rf['discharge_cms']

	# Drop NaN
	all_data = all_data.dropna()

	# Convert to liters per second
	all_data = all_data.mul(1000.0)

	# Compute change in discharge
	all_data['TSD_DELTA'] = all_data['TSD'].sub(all_data['RAW'])
	all_data['MAN_DELTA'] = all_data['MAN'].sub(all_data['RAW'])

	# Plot options
	label = f'delta_Q'
	caption = r'Box-and-whisker plots of differences in discharge ($\Delta Q$) between corrected and uncorrected discharges for automatically corrected (TSD) and manually corrected (MAN) data binned by corrected discharge ($Q_c$) with number of total 5-minute measurements in each bin (N). Boxes indicate the interquartile range. Whiskers indicate the inner 95th percentile. Individual points indicate outliers. Horizontal lines indicate the median. Black squares indicate the mean. Bins: \SI{< 10.0}{\liter\per\second}, \SIrange{10.0}{ <100.0}{\liter\per\second}, \SIrange{100.0}{ <1000.0}{\liter\per\second}, \SI{ >1000.0}{\liter\per\second}.'

	# Get LaTex figure
	fig, axs = get_figure(size='large', ncols=2, nrows=2)
	bps = [None, None, None, None]

	# Styling
	mean_props = dict(marker='s', markeredgecolor='black', 
		markerfacecolor='black', markersize=4)
	median_props = dict(color='black')
	labels = ['TSD', 'MAN']

	# Plot
	ax = axs.flat[0]
	ax.grid(False)
	subdata_a = all_data[all_data.TSD < 10.0]
	subdata_m = all_data[all_data.MAN < 10.0]
	subdata = [subdata_a.TSD_DELTA, subdata_m.MAN_DELTA]
	bps[0] = ax.boxplot(subdata, patch_artist=True, showmeans=True, 
		meanprops=mean_props, medianprops=median_props, labels=labels, 
		whis=[2.5, 97.5])
	ax.set_ylabel(r'Correction, $\Delta Q$ ($Ls^{-1}$)')
	ax.set_title(r'(a) $Q_c <10 Ls^{-1}$')
	ax.set_ylim(-15, 15)
	ax.set_yticks(np.arange(-15, 20, 5))

	# Add counts
	N_a = subdata_a['TSD_DELTA'].count()
	N_m = subdata_m['MAN_DELTA'].count()

	# Note errors
	ax.annotate(f'N = {N_a}', (0.20, 0.85), xycoords='axes fraction')
	ax.annotate(f'N = {N_m}', (0.70, 0.85), xycoords='axes fraction')

	# Plot
	ax = axs.flat[1]
	ax.grid(False)
	subdata_a = all_data[(all_data.TSD >= 10.0) & (all_data.TSD < 100.0)]
	subdata_m = all_data[(all_data.MAN >= 10.0) & (all_data.MAN < 100.0)]
	subdata = [subdata_a.TSD_DELTA, subdata_m.MAN_DELTA]
	bps[1] = ax.boxplot(subdata, patch_artist=True, showmeans=True, 
		meanprops=mean_props, medianprops=median_props, labels=labels, 
		whis=[2.5, 97.5])
	ax.set_ylabel(r'Correction, $\Delta Q$ ($Ls^{-1}$)')
	ax.set_title(r'(b) $Q_c = 10$ to $<100 Ls^{-1}$')
	ax.set_ylim(-80, 80)
	ax.set_yticks(np.arange(-80, 100, 40))

	# Add counts
	N_a = subdata_a['TSD_DELTA'].count()
	N_m = subdata_m['MAN_DELTA'].count()

	# Note errors
	ax.annotate(f'N = {N_a}', (0.20, 0.85), xycoords='axes fraction')
	ax.annotate(f'N = {N_m}', (0.70, 0.85), xycoords='axes fraction')

	# Plot
	ax = axs.flat[2]
	ax.grid(False)
	subdata_a = all_data[(all_data.TSD >= 100.0) & (all_data.TSD < 1000.0)]
	subdata_m = all_data[(all_data.MAN >= 100.0) & (all_data.MAN < 1000.0)]
	subdata = [subdata_a.TSD_DELTA, subdata_m.MAN_DELTA]
	bps[2] = ax.boxplot(subdata, patch_artist=True, showmeans=True, 
		meanprops=mean_props, medianprops=median_props, labels=labels, 
		whis=[2.5, 97.5])
	ax.set_ylabel(r'Correction, $\Delta Q$ ($Ls^{-1}$)')
	ax.set_title(r'(c) $Q_c = 100$ to $<1000 Ls^{-1}$')
	ax.set_ylim(-260, 260)
	ax.set_yticks(np.arange(-260, 390, 130))

	# Add counts
	N_a = subdata_a['TSD_DELTA'].count()
	N_m = subdata_m['MAN_DELTA'].count()

	# Note errors
	ax.annotate(f'N = {N_a}', (0.30, 0.85), xycoords='axes fraction')
	ax.annotate(f'N = {N_m}', (0.70, 0.85), xycoords='axes fraction')

	# Plot
	ax = axs.flat[3]
	ax.grid(False)
	subdata_a = all_data[all_data.TSD > 1000.0]
	subdata_m = all_data[all_data.MAN > 1000.0]
	subdata = [subdata_a.TSD_DELTA, subdata_m.MAN_DELTA]
	bps[3] = ax.boxplot(subdata, patch_artist=True, showmeans=True, 
		meanprops=mean_props, medianprops=median_props, labels=labels, 
		whis=[2.5, 97.5])
	ax.set_ylabel(r'Correction, $\Delta Q$ ($Ls^{-1}$)')
	ax.set_title(r'(d) $Q_c >1000 Ls^{-1}$')
	ax.set_ylim(-500, 500)
	ax.set_yticks(np.arange(-500, 750, 250))

	# Add counts
	N_a = subdata_a['TSD_DELTA'].count()
	N_m = subdata_m['MAN_DELTA'].count()

	# Note errors
	ax.annotate(f'N = {N_a}', (0.20, 0.85), xycoords='axes fraction')
	ax.annotate(f'N = {N_m}', (0.70, 0.85), xycoords='axes fraction')

	# fill with colors
	colors = ['C5', 'C9']
	for bp in bps:
		for patch, flier, color in zip(bp['boxes'], bp['fliers'], colors):
			patch.set_facecolor(color)
			flier.set_marker('o')
			flier.set_markerfacecolor(color)
			flier.set_markeredgecolor(color)
			flier.set_markersize(2)

	# Format plot and save
	write_figure(fig, label, caption)
