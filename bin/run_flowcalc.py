'''Convert stage to discharge'''

# Modules
from pathlib import Path
import shutil
import os
from subprocess import run
import pandas as pd
from datetime import datetime as dt

# Convert string to datetime
def _parse(dts):
	return dt.strptime(dts, '%m/%d/%Y %H:%M:%S')

# Catchment area in hectares
_area = 144.0

# Header for all files
header = '''Time Series Data: 
Date/Time,Value,Lock Status
--------
'''

# Temp directory
TMP_DIR = Path('/tmp/error_correction/discharge')
TMP_DIR.mkdir(exist_ok=True, parents=True)

# Set document root
root = Path(__file__).parents[1].resolve()

# Flowcalc location
FLOWCALC = root / 'bin' / 'flowcalc' / 'flowcalc'
shutil.copy2(str(FLOWCALC), TMP_DIR)

if __name__ == '__main__':

	# Input data
	dfile = root / 'data' / 'V02FORH_DFCS.pickle'
	mfile = root / 'data' / 'V02FORH_MAN.pickle'
	rfile = root / 'data' / 'FOR_rain.pickle'

	# Load data
	df = pd.read_pickle(dfile)
	mf = pd.read_pickle(mfile)
	rf = pd.read_pickle(rfile)

	# Trim manual data
	start = df.index.min()
	stop = df.index.max()
	mf = mf.loc[start:stop, :]

	# Combine
	final = pd.DataFrame()
	final['TSD'] = df['corrected']
	final['MAN'] = mf['value']
	final['RAW'] = df['value']
	final['Lock Status'] = 'Unlocked'
	final.loc[df['locked'], 'Lock Status'] = 'Locked'

	# Gage specific details
	weir = dfile.stem[:3]
	site = dfile.stem[3:6]
	height = dfile.stem[6]

	# Initial working directory
	wk_dir = Path.cwd()

	# Change working dir to /tmp
	os.chdir(str(TMP_DIR))

	# TSV files
	tsv_files = []

	# Convert stage to runoff
	for mtd in ['TSD', 'MAN', 'RAW']:
		# input file path
		ifile = (TMP_DIR / '{}AS{}{}{}.csv'.format(weir, height, mtd, 
			site))

		# output file path
		ofile = Path(str(ifile).replace('.csv', '.tsv'))
		tsv_files.append(ofile)

		# Write output
		output = header
		output += final[[mtd, 'Lock Status']].to_csv(header=False, 
			date_format='%-m/%-d/%Y %H:%M:%S')
		with ifile.open('w') as f:
			f.write(output)

		# Construct command
		command = ([
			'./{}'.format(FLOWCALC.name), 
			'-{}'.format(height.lower()), ifile.name, 
			'-o', ofile.name
			])

		# Convert to discharge
		run(command)

	# Return to original working dir
	os.chdir(str(wk_dir))

	# Read tsv files
	for file in tsv_files:
		# Grab catchment code
		code = file.stem[-3:]

		# Load data
		df = pd.read_csv(file, sep='\s+', comment='#', header=None, names=['date', 
				'time', 'discharge_cms', 'flag'])

		# Compute timestamp
		df['dts'] = df['date'].add(' ').add(df['time'])
		df['datetime'] = df['dts'].apply(_parse)

		# Clean-up
		df = df[['datetime', 'discharge_cms']].set_index('datetime')

		# Convert to 15-min runoff in mm
		df = df.resample('15min').mean().interpolate(method='time')
		df['runoff_mm'] = df['discharge_cms'].mul(90.0/_area)

		# Add rain data
		df['rainfall_mm'] = rf.loc[df.index[0]:df.index[-1], 'rainfall']

		# Save data
		ofile = root / 'data' / f'{file.stem}_RUNOFF.pickle'
		df.to_pickle(ofile)