''' Perform recession analysis and baseflow separation'''


# Libraries
from pathlib import Path
import numpy as np
import pandas as pd

# Constants
win_size = 3	# Must be positive odd integer >= 3
err = 0.02		# Measurement error

# Compute baseflow recession constant
def bf_rec(ts):
	# Resample
	df = ts.resample('D').sum()

	# Drop last day -- incomplete
	df = df.iloc[:-1]

	# Note missing values
	df['ignore'] = df['runoff_mm'].isnull()

	# Assign numerical index
	df['idx'] = [i for i in range(len(df.index))]

	# Note differences
	df['decrease'] = df['runoff_mm'].diff()

	# Storage for baseflow values
	yk = []
	ykp = []

	# Collect baseflow values
	initial = int(win_size / 2)
	final = int(len(df.index) - initial)
	for idx in range(initial,final):
		# Generate window
		window = [idx + i - initial for i in range(win_size)]
		
		# Grab slice
		df2 = df[df['idx'].isin(window)]

		# Check for NaN
		if df2['ignore'].any():
			continue

		# Check for baseflow
		if (df2['decrease'] < 0.0).all():
			# Store
			yk.append(df.loc[df.idx == idx, 'runoff_mm'].values[0])
			ykp.append(df.loc[df.idx == idx+1, 'runoff_mm'].values[0])

	# Limit linear recession coefficient
	yk = np.array(yk)
	ykp = np.array(ykp)
	recs = pd.DataFrame()
	recs['yk'] = yk
	recs['ykp'] = ykp
	recs['a'] = recs['ykp'].div(recs['yk'])
	a = [0.0]

	while True:
		# Determine linear reservoir coefficient
		x = recs['yk'].values[:,np.newaxis]
		y = recs['ykp'].values
		a, _, _, _ = np.linalg.lstsq(x, y, rcond=1)

		# Predict
		recs.loc[:, 'model'] = recs['yk'].mul(a[0])

		# Compute error
		recs.loc[:, 'error'] = recs['ykp'].sub(
			recs['model']).div(recs['model'])

		# Check for outliers
		outliers = recs[recs['error'] > err]
		if outliers.empty:
			break

		# Drop minimum value
		drop = recs['a'].idxmin()
		recs = recs.drop(drop, axis=0)

	# Return constant
	return a[0], len(yk)

# Use reverse filtering to compute BFI_max
def bfi_max(ts, a):
	# Resample
	df = ts.resample('D').sum()

	# Perform reverse pass
	r_value = df['runoff_mm'].values[::-1]
	baseflow = [r_value[0]]
	for idx in range(1, len(r_value)):
		val = min(r_value[idx], r_value[idx-1]/a)
		baseflow.append(val)
	df['baseflow'] = baseflow[::-1]

	# Compute BFI_max
	return df['baseflow'].sum()/df['runoff_mm'].sum()

# Generate baseflow
def baseflow(ts, a, bfi_max):
	# Resample
	df = ts.resample('D').sum()

	# Separate
	y = df['runoff_mm'].values
	baseflow = [y[0]]
	for idx in range(1, len(df['runoff_mm'].values)):
		# Eckhardt filter
		bk = ((((1.0 - bfi_max) * a * baseflow[idx-1]) + 
			((1.0 - a) * bfi_max * y[idx])) / 
				(1.0 - a * bfi_max))

		# Enforce maximum
		val = min(bk, y[idx])

		# Enforce minimum
		bfi = val / y[idx]
		if bfi >= bfi_max:
			val = y[idx]

		# Store
		baseflow.append(val)

	# Compute runoff efficiency
	df['baseflow'] = baseflow
	df['direct_ro'] = df['runoff_mm'].sub(df['baseflow'])
	return (df['direct_ro'].sum()/df['rainfall_mm'].sum()) * 100.

if __name__ == '__main__':
	# Set document root
	root = Path(__file__).parents[1].resolve()

	# Input data
	dfile = root / 'data' / 'V02ASHDECFOR_RUNOFF.pickle'
	mfile = root / 'data' / 'V02ASHMANFOR_RUNOFF.pickle'

	# Load data
	df = pd.read_pickle(dfile)
	mf = pd.read_pickle(mfile)

	# Compute baseflow recession constants
	dfa, dfn = bf_rec(df)
	mfa, mfn = bf_rec(mf)

	# Find maximum BFIs
	d_bfi = bfi_max(df, dfa)
	m_bfi = bfi_max(mf, mfa)

	# Separate baseflow and compute runoff efficiency
	d_re = baseflow(df, dfa, d_bfi)
	m_re = baseflow(mf, mfa, m_bfi)
	
	# Show results
	print('TSD - a: {:.4f}, b: {:.2f}, n: {:d}, re: {:.2f}'.format(dfa, d_bfi, 
		dfn, d_re))
	print('MAN - a: {:.4f}, b: {:.2f}, n: {:d}, re: {:.2f}'.format(mfa, m_bfi, 
		mfn, m_re))