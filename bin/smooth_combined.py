#!/usr/bin/env python3

'''Lightly smooth final signal.'''

# Modules
from pathlib import Path
from datetime import timedelta as td
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tools.figures import get_figure, write_figure
from matplotlib.ticker import FormatStrFormatter
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as dates

# Use package style sheet
style_sheet = Path(__file__).parents[0] / 'tools/matplotlibrc'
style_sheet = style_sheet.resolve()
plt.style.use(str(style_sheet))

# Smooth time-series
def smooth_signal(ts, sp):
	df = pd.DataFrame()
	df['value'] = ts
	df['rvalue'] = df['value'].values[::-1]
	df['backward'] = df['rvalue'].ewm(
		span=sp, adjust=False).mean().values[::-1]
	df['forward'] = df['value'].ewm(span=sp, adjust=False).mean()
	return df[['backward', 'forward']].mean(axis=1)

# Rolling minimum
def minimize_signal(ts, win):
	df = pd.DataFrame()
	df['value'] = ts
	df['rvalue'] = df['value'].values[::-1]
	df['backward'] = df['rvalue'].rolling(
		win).min().values[::-1]
	df['forward'] = df['value'].rolling(win).min()
	return df[['backward', 'forward']].max(axis=1)

# Main
if __name__ == '__main__':
	# Set document root
	root = Path(__file__).parents[1].resolve()

	# Input data
	dfile = root / 'data' / 'V02FORH_DFC.pickle'

	# Load data
	df = pd.read_pickle(dfile)

	# Approximate and smooth baseflow
	df['bf'] = minimize_signal(df['corrected'], 'D')
	df['bfi'] = df['bf'].div(df['corrected'])
	df.loc[df.bfi >= 0.95, 'bf'] = df.loc[df.bfi >= 0.95, 'corrected']
	df.loc[df.bfi < 0.95, 'bf'] = np.nan
	df.loc[:, 'bf'] = df['bf'].interpolate()
	df['bf12'] = smooth_signal(df['bf'], 12)
	df['bf72'] = smooth_signal(df['bf'], 72)
	df.loc[df.bf12 < df.bf72, 'corrected'] = (
		df.loc[df.bf12 < df.bf72, 'corrected'].sub(
			df.loc[df.bf12 < df.bf72, 'bf12']).add(
			df.loc[df.bf12 < df.bf72, 'bf72']))

	# Output data
	ofile = root / 'data' / 'V02FORH_DFCS.pickle'
	df.to_pickle(ofile)

	# Compute better y-limits for plotting
	ymin = df['value'].quantile(0.05) * 0.95
	ymax = df['value'].max() * 1.05

	# Plot options
	label = f'manned_corrected_data'
	caption = r'(a) Raw and manually (MAN) corrected stage data using a time series editor. (b) Raw and automatically (TSD) corrected stage data using time series decomposition.'

	# Get LaTex figure
	fig, ax = get_figure(size='large_wide', nrows=1, ncols=1)

	# Compare to manually processed data
	man_file = root / 'data' / 'V02FORH_MAN.pickle'
	mf = pd.read_pickle(man_file)
	# mf["stub"] = np.nan

	# Left plot
	# df[['value']].plot(ax=ax, logy=True)
	# mf[['value']].plot(ax=ax, color='C9')
	# df.loc[df.locked, ['value']].plot(ax=ax, linestyle='', 
	# 	marker='o', color='C4', markeredgecolor='black')
	# ax.grid(True, which='both', axis='y')
	# ax.yaxis.set_major_formatter(
	# 	FormatStrFormatter('%.1f'))
	# ax.set_ylim(ymin, ymax)
	# ax.set_xlabel('Datetime (UTC)')
	# ax.set_ylabel('Depth behind Weir (mm$H_2O$)')
	# # ax.legend(['Recorded Sensor Depth', 'MAN Corrected Depth', 
	# # 	'Field Observation'])
	# ax.get_legend().remove()
	# ax.set_title("(a)")

	# Plot
	df[['value']].plot(ax=ax, logy=True)
	mf[['value']].plot(ax=ax, color='C9')
	df[['corrected']].plot(ax=ax, color='C5')
	df.loc[df.locked, ['value']].plot(ax=ax, linestyle='', 
		marker='o', color='C4', markeredgecolor='black')
	ax.grid(True, which='both', axis='y')
	ax.yaxis.set_major_formatter(
		FormatStrFormatter('%.1f'))
	ax.set_ylim(ymin, ymax)
	ax.set_xlabel('Datetime (UTC)')
	ax.set_ylabel('Depth behind Weir (mm$H_2O$)')
	# ax.get_legend().remove()
	# ax.set_title("(b)")

	# mf[['stub']].plot(ax=axs[2])
	# mf[['stub']].plot(ax=axs[2], color='C9')
	# mf[['stub']].plot(ax=axs[2], color='C5')
	# mf[['stub']].plot(ax=axs[2], linestyle='', 
	# 	marker='o', color='C4', markeredgecolor='black')
	ax.legend(['RAW', 'MAN', 'TSD', 
		'Field Measurement'])
	# axs[2].set_title("(b)")

	# axs[2].axis("off")
	# axs[3].axis("off")

	# Format plot and save
	write_figure(fig, label, caption)
