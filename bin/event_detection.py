#!/usr/bin/env python3

'''Isolate and plot runoff events.'''

# Modules
from pathlib import Path
from datetime import timedelta as td
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tools.figures import get_figure, write_figure
from matplotlib.ticker import FormatStrFormatter
from matplotlib.ticker import MultipleLocator

# Use package style sheet
style_sheet = Path(__file__).parents[0] / 'tools/matplotlibrc'
style_sheet = style_sheet.resolve()
plt.style.use(str(style_sheet))

# Smooth time-series
def smooth_signal(ts, sp):
	df = pd.DataFrame()
	df['value'] = ts
	df['rvalue'] = df['value'].values[::-1]
	df['backward'] = df['rvalue'].ewm(
		span=sp, adjust=False).mean().values[::-1]
	df['forward'] = df['value'].ewm(span=sp, adjust=False).mean()
	return df[['backward', 'forward']].mean(axis=1)

# Rolling minimum
def minimize_signal(ts, win):
	df = pd.DataFrame()
	df['value'] = ts
	df['rvalue'] = df['value'].values[::-1]
	df['backward'] = df['rvalue'].rolling(
		win).min().values[::-1]
	df['forward'] = df['value'].rolling(win).min()
	return df[['backward', 'forward']].max(axis=1)

# Main
if __name__ == '__main__':
	# Set document root
	root = Path(__file__).parents[1].resolve()

	# Input data
	dfile = root / 'data' / 'V02FORH_raw.pickle'

	# Output data
	ofile = root / 'data' / 'V02FORH_events.pickle'

	# Load data
	df = pd.read_pickle(dfile)

	# Smooth series
	df['hourly'] = smooth_signal(df['value'], 12)

	# Identify base-flow trend
	df['trend'] = minimize_signal(df['hourly'], 'D')

	# Remove trend
	df['detrend'] = df['hourly'].sub(df['trend'])

	# Daily periodic component (ET)
	df['periodic'] = smooth_signal(df['detrend'], 24)

	# Remove periodicity
	df['regular'] = np.abs(df['detrend'].sub(df['periodic']))

	# Scale smoothing window by upper flow values
	w = (np.ceil(df['detrend'].quantile(0.9) * 0.618 - 2.21))
	win = td(hours=int(w))

	# Smooth final signal
	df['smooth'] = df['regular'].rolling(win).mean()

	# Eliminate small values
	limit = df['smooth'].max() * 0.02
	df.loc[df.smooth < limit, 'smooth'] = 0.0

	# Mark events
	df['event'] = False
	df.loc[df.smooth != 0.0, 'event'] = True

	# Compute better y-limits for plotting
	ymin = df['value'].quantile(0.05) * 0.95
	ymax = df['value'].max() * 1.05

	# Plot options
	label = f'direct_runoff'
	caption = r'Graphical example of progressive signal decomposition and removal. These data directly correspond to the stage record shown in Figure \ref{fig:raw_data}. "Baseflow removed" values are the original stage values with erroneous baseflow removed. The "Periodicity removed" values are the original stage values with baseflow and daily periodic components removed. "Noise removed" values are the remaining stage values after removing baseflow, daily periodic components, and noise. The non-zero "Direct runoff" values correspond to times of direct runoff associated with storm events.'

	# Get LaTex figure
	fig, ax = get_figure(size='large')

	# Plot
	df[['detrend', 'regular', 'smooth']].plot(ax=ax)
	ax.grid(False)
	ax.yaxis.set_major_formatter(
		FormatStrFormatter('%.1f'))
	ax.set_xlabel('Datetime (UTC)')
	ax.set_ylabel('Depth (mm$H_2O$)')
	ax.legend(['Baseflow removed', 'Periodicity removed', 
		'Direct runoff'])

	# Format plot and save
	write_figure(fig, label, caption)

	# Plot options
	label = f'event_detection'
	caption = r'Resulting storm event depths after time series decomposition. We removed events less than 6-hours in total duration and events with rise or recession durations in the lower 5\% of all events. We merged periods of baseflow recession less than 3-hours into surrounding events and selected local minima within 3-hours of event initiations as event start times.'

	# Get LaTex figure
	fig, ax = get_figure(size='large')

	# Plot options
	label = f'trend'
	caption = r'Modeled erroneous baseflow using local minimums to filter smoothed raw stage data. We generated the smoothed record using an hourly EWMA (smoothing parameter $\lambda \approx 0.154$ for 5-minute data) on raw stage values to reduce the influence of noise. We estimated baseflow (daily minimum depth) by taking the maximum of forward and backward daily minimums of the smoothed record.'

	# Get LaTex figure
	fig, ax = get_figure(size='large')

	# Plot
	df[['value', 'trend']].plot(ax=ax, logy=True)
	df.loc[df.locked, ['value']].plot(ax=ax, linestyle='', 
		marker='o', color='C4', markeredgecolor='black')
	ax.grid(True, which='both', axis='y')
	ax.yaxis.set_major_formatter(
		FormatStrFormatter('%.1f'))
	ax.set_ylim(ymin, ymax)
	ax.set_xlabel('Datetime (UTC)')
	ax.set_ylabel('Depth behind Weir (mm$H_2O$)')
	ax.legend(['Recorded Sensor Depth', 'Uncorrected Baseflow', 
		'Field Observed Depth'])

	# Format plot and save
	write_figure(fig, label, caption)

	# Save event data
	df.to_pickle(ofile)
