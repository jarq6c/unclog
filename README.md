# Automated Correction of Systematic Errors in High Frequency Stage Data from V‑Notch Weirs using Time Series Decomposition

Source code demonstrating a workflow to correct systematic errors in stage data behind V-notch weirs.
