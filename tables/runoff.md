| Statistic                      |   AUT |   MAN |           HIS |
|:-------------------------------|------:|------:|--------------:|
| Number of recessions           |   127 |   141 |           757 |
| Recession constant             |  0.98 |  0.98 |          0.97 |
| Maximum baseflow index         |  0.81 |  0.81 |          0.84 |
| Annual storm runoff efficiency |  10.6 |  10.9 |  8.45 to 11.1 |

: Baseflow recession coefficients and annual storm runoff efficiencies derived from runoff data corrected using time series decomposition (AUT), manual correction (MAN), and historical data (HIS). {#tbl:recessions}
