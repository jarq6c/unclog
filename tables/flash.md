| Index       |       TSD |       MAN |   HIS |      RAW |
|:------------|----------:|----------:|------:|---------:|
| $F_{10/90}$ | 17.9599   | 20.8812   | 20.1  | 50.7404  |
| $F_{20/80}$ |  8.93215  |  8.468    |  6.9  | 10.0169  |
| $F_{25/75}$ |  5.84046  |  5.94134  |  5.2  |  6.0409  |
| $F_{.5}$    |  2.13628  |  1.83193  |  1.5  |  1.88868 |
| $F_{.6}$    |  2.67156  |  2.32008  |  1.8  |  2.53232 |
| $F_{.8}$    |  4.04165  |  4.1091   |  3.5  |  4.65066 |
| CVLF5       |  0.900125 |  0.955411 |  1.56 |  1.0083  |

: Runoff flashiness indices derived from automatically corrected (TSD), manually corrected (MAN), historical (HIS), and raw (RAW) stage data. {#tbl:flash}
