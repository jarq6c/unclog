# Modules
from pathlib import Path
from subprocess import run

# Round plot limit to nice value
def ceil_base(num, base=10):
	factor = np.ceil(num / base)
	return factor * base

# Round plot limit to nice value (base 10)
def floor10(num):

	magnitude = np.floor(np.log10(num))
	return 10.0 ** magnitude

# Round plot limit to nice value (base 10)
def ceil10(num):
	magnitude = np.ceil(np.log10(num))
	return 10.0 ** magnitude

# Main
if __name__ == '__main__':
	# Find root directory
	root = Path(__file__).parents[0].resolve()

	# Set environment
	env = str(root / 'env/bin/python')

	# Script list
	scripts = ([
		str(root / 'bin/delta_method.py'),
		str(root / 'bin/plot_data.py'),
		str(root / 'bin/event_detection.py'),
		str(root / 'bin/refine_events.py'),
		str(root / 'bin/inspect_signal.py'),
		str(root / 'bin/clean_signal.py'),
		str(root / 'bin/compare_parameters.py'),
		str(root / 'bin/model_baseflow.py'),
		str(root / 'bin/reconstruct.py'),
		str(root / 'bin/smooth.py'),
		str(root / 'bin/compare_results.py'),
		str(root / 'bin/detailed_results.py'),
		str(root / 'bin/run_flowcalc.py'),
		str(root / 'bin/flashiness.py'),
		str(root / 'bin/separation.py'),
		str(root / 'bin/delta_Q.py')
		])

	# Run
	for script in scripts:
		run([env, script])
	